import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
import io
import base64


#Right ankle steps in respect to time
def right_ankle_steps_time(RawStepData,RawTimeData):

    fig = plt.figure()

    steps = RawStepData.split(",")
    time = RawTimeData.split(",")

    stepData = []
    timeData = []

    for i in steps:
        stepData.append(int(i))
    for i in time:
        timeData.append(int(i))
        
    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Time in seconds")
    ay.set_ylabel("Total steps")
    ay.set_title("Right Ankle Steps in Respect to Time")
    ay.plot(timeData,stepData)
    
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')




#Right ankle X, Y and Z rotations in respect to time
def right_ankle_rotations_time(RawRotXData,RawRotYData,RawRotZData,RawTimeData):

    fig = plt.figure()

    rotX = RawRotXData.split(",")
    rotY = RawRotYData.split(",")
    rotZ = RawRotZData.split(",")
    time = RawTimeData.split(",")

    rotXData = []
    rotYData = []
    rotZData = []
    timeData = []

    for i in rotX:
        rotXData.append(float(i))
    for i in rotY:
        rotYData.append(float(i))
    for i in rotZ:
        rotZData.append(float(i))
    for i in time:
        timeData.append(int(i))
        
    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Time in seconds")
    ay.set_ylabel("Rotation")
    ay.set_title("Right Ankle Rotations in Respect to Time")
    ay.plot(timeData,rotXData)
    ay.plot(timeData,rotYData)
    ay.plot(timeData,rotZData)
    ay.legend(['X-axis','Y-axis','Z-axis'])
    
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')


#Left ankle steps in respect to time
def left_ankle_steps_time(RawStepData,RawTimeData):

    fig = plt.figure()

    steps = RawStepData.split(",")
    time = RawTimeData.split(",")

    stepData = []
    timeData = []

    for i in steps:
        stepData.append(int(i))
    for i in time:
        timeData.append(int(i))
        
    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Time in seconds")
    ay.set_ylabel("Total steps")
    ay.set_title("Left Ankle Steps in Respect to Time")
    ay.plot(timeData,stepData)
    
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')



#Left ankle X, Y and Z rotations in respect to time
def left_ankle_rotations_time(RawRotXData,RawRotYData,RawRotZData,RawTimeData):

    fig = plt.figure()

    rotX = RawRotXData.split(",")
    rotY = RawRotYData.split(",")
    rotZ = RawRotZData.split(",")
    time = RawTimeData.split(",")

    rotXData = []
    rotYData = []
    rotZData = []
    timeData = []

    for i in rotX:
        rotXData.append(float(i))
    for i in rotY:
        rotYData.append(float(i))
    for i in rotZ:
        rotZData.append(float(i))
    for i in time:
        timeData.append(int(i))
        
    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Time in seconds")
    ay.set_ylabel("Rotation")
    ay.set_title("Left Ankle Rotations in Respect to Time")
    ay.plot(timeData,rotXData)
    ay.plot(timeData,rotYData)
    ay.plot(timeData,rotZData)
    ay.legend(['X-axis','Y-axis','Z-axis'])
    
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')


#Left ankle X, Y and Z rotations in respect to steps
def left_ankle_rotations_steps(RawRotXData,RawRotYData,RawRotZData,RawStepData):

    fig = plt.figure()

    rotX = RawRotXData.split(",")
    rotY = RawRotYData.split(",")
    rotZ = RawRotZData.split(",")
    steps = RawStepData.split(",")

    rotXData = []
    rotYData = []
    rotZData = []
    stepData = []

    for i in rotX:
        rotXData.append(float(i))
    for i in rotY:
        rotYData.append(float(i))
    for i in rotZ:
        rotZData.append(float(i))
    for i in steps:
        stepData.append(int(i))
        
    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Total Amount of Steps")
    ay.set_ylabel("Rotation")
    ay.set_title("Left Ankle Rotations in Respect to steps")
    #ay.plot(stepData,rotXData)
    #ay.plot(stepData,rotYData)
    ay.plot(stepData,rotZData)
    #ay.legend(['X-axis','Y-axis','Z-axis'])
    ay.legend(['Z-axis'])
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')


#Right ankle X, Y and Z rotations in respect to steps
def right_ankle_rotations_steps(RawRotXData,RawRotYData,RawRotZData,RawStepData):

    fig = plt.figure()

    rotX = RawRotXData.split(",")
    rotY = RawRotYData.split(",")
    rotZ = RawRotZData.split(",")
    steps = RawStepData.split(",")

    rotXData = []
    rotYData = []
    rotZData = []
    stepData = []

    for i in rotX:
        rotXData.append(float(i))
    for i in rotY:
        rotYData.append(float(i))
    for i in rotZ:
        rotZData.append(float(i))
    for i in steps:
        stepData.append(int(i))
        
    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Total Amount of Steps")
    ay.set_ylabel("Rotation")
    ay.set_title("Right Ankle Rotations in Respect to steps")
    ay.plot(stepData,rotXData)
    ay.plot(stepData,rotYData)
    ay.plot(stepData,rotZData)
    ay.legend(['X-axis','Y-axis','Z-axis'])
    
    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')


#Left ankle X, Y and Z rotations in respect to time
def swaying_over_time(RawRotXData,RawRotYData):

    fig = plt.figure()

    rotX = RawRotXData.split(",")
    rotY = RawRotYData.split(",")


    rotXData = []
    rotYData = []


    for i in rotX:
        rotXData.append(float(i))
    for i in rotY:
        rotYData.append(float(i))


    ay = fig.add_subplot(1,1,1)
    ay.set_xlabel("Swaying around the Y axis [degrees]")
    ay.set_ylabel("Swaying around the X axis [degrees]")
    ay.set_title("Swaying Path over the duration of the Session")
    ay.plot(rotXData,rotYData)

    ay.legend(['Path'])

    fig.canvas.draw()
    img = np.fromstring(fig.canvas.tostring_rgb(),dtype=np.uint8,sep='')
    img = img.reshape(fig.canvas.get_width_height()[::-1]+(3,))
    img = cv2.cvtColor(img,cv2.COLOR_RGB2BGR)
    pil_im = Image.fromarray(img)
    buff = io.BytesIO()
    pil_im.save(buff,format="PNG")
    img_str = base64.b64encode(buff.getvalue())
    return ""+str(img_str,'utf-8')








