package com.example.vertigodiagnosticapplication.models;

/**
 * Created by lipponep on 22.11.2017.
 */

import com.google.gson.annotations.SerializedName;

public class DataResponse {

    @SerializedName("Body")
    public final Body body;

    @SerializedName("Uri")
    public String uri;

    public DataResponse(Body body, String uri) {
        this.body = body;
        this.uri = uri;

    }

    public static class Body {
        @SerializedName("Timestamp")
        public final long timestamp;

        @SerializedName("ArrayAcc")
        public final AccArray acc_array[];

        @SerializedName("ArrayGyro")
        public final GyroArray[] gyr_array;

        @SerializedName("ArrayMagn")
        public final MagArray[] mag_array;

        @SerializedName("Headers")
        public final Headers header;

        public Body(long timestamp, AccArray[] acc_array, GyroArray[] gyr_array, MagArray[] mag_array, Headers header) {
            this.timestamp = timestamp;
            this.acc_array = acc_array;
            this.gyr_array = gyr_array;
            this.mag_array = mag_array;
            this.header = header;
        }
    }

//  {"Body": {"Timestamp": 18574, "ArrayAcc": [{"x": -0.074180148541927338, "y": -0.059822700917720795, "z": -9.7319574356079102}, {"x": -0.07657305896282196, "y": -0.090930506587028503, "z": -9.7056350708007812}, {"x": -0.062215611338615417, "y": -0.093323417007923126, "z": -9.7223854064941406}, {"x": -0.047858163714408875, "y": -0.055036887526512146, "z": -9.7247781753540039}, {"x": -0.026321988552808762, "y": -0.090930506587028503, "z": -9.7008495330810547}, {"x": -0.062215611338615417, "y": -0.028714897111058235, "z": -9.6864919662475586}, {"x": -0.090930506587028503, "y": -0.052643977105617523, "z": -9.6745271682739258}, {"x": -0.03828652948141098, "y": -0.071787245571613312, "z": -9.7367429733276367}], "ArrayGyro": [{"x": 0.84000003337860107, "y": -3.0799999237060547, "z": -0.98000001907348633}, {"x": 0.84000003337860107, "y": -3.1500000953674316, "z": -1.0499999523162842}, {"x": 0.69999998807907104, "y": -3.1500000953674316, "z": -1.1200000047683716}, {"x": 0.76999998092651367, "y": -3.2200000286102295, "z": -1.0499999523162842}, {"x": 0.76999998092651367, "y": -3.1500000953674316, "z": -0.98000001907348633}, {"x": 0.76999998092651367, "y": -3.0799999237060547, "z": -0.98000001907348633}, {"x": 0.69999998807907104, "y": -3.0799999237060547, "z": -1.0499999523162842}, {"x": 0.76999998092651367, "y": -3.1500000953674316, "z": -0.98000001907348633}], "ArrayMagn": [{"x": 4.58758544921875, "y": 35.699352264404297, "z": 76.3905029296875}, {"x": 1.3579769134521484, "y": 34.1204833984375, "z": 75.571517944335938}, {"x": 2.4259998798370361, "y": 36.871471405029297, "z": 77.033317565917969}, {"x": 4.1088261604309082, "y": 35.275341033935547, "z": 78.281150817871094}, {"x": 2.4901869297027588, "y": 35.285057067871094, "z": 78.153526306152344}, {"x": 3.8261837959289551, "y": 34.254268646240234, "z": 77.541297912597656}, {"x": 5.6297421455383301, "y": 31.564426422119141, "z": 77.753326416015625}, {"x": 1.7660613059997559, "y": 34.048828125, "z": 75.4158935546875}]}, "Uri": "213130000630/Meas/IMU9/104", "Method": "PUT"}

    public static class AccArray {
        @SerializedName("x")
        public final double x;

        @SerializedName("y")
        public final double y;

        @SerializedName("z")
        public final double z;

        public AccArray(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }
    public static class GyroArray {
        @SerializedName("x")
        public final double x;

        @SerializedName("y")
        public final double y;

        @SerializedName("z")
        public final double z;

        public GyroArray(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }

    }
    public static class MagArray {
        @SerializedName("x")
        public final double x;

        @SerializedName("y")
        public final double y;

        @SerializedName("z")
        public final double z;

        public MagArray(double x, double y, double z) {
            this.x = x;
            this.y = y;
            this.z = z;
        }
    }

    public static class Headers {
        @SerializedName("Param0")
        public final int param0;

        public Headers(int param0) {
            this.param0 = param0;
        }
    }
}
