package com.example.vertigodiagnosticapplication.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.vertigodiagnosticapplication.R;

import java.util.ArrayList;

public class GraphNameAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> GraphNames;
    LayoutInflater inflater;

    public GraphNameAdapter(Context context, ArrayList<String> GraphNames){
        this.context = context;
        this.GraphNames = GraphNames;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return GraphNames.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflater.inflate(R.layout.item_graphname,null);
        TextView txtView = (TextView) view.findViewById(R.id.graphName);
        txtView.setText(GraphNames.get(i));
        return view;
    }
}
