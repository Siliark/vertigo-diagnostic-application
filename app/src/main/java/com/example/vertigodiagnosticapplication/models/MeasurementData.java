package com.example.vertigodiagnosticapplication.models;

import com.example.vertigodiagnosticapplication.misc.MadgwickAHRS;

import java.util.ArrayList;

public class MeasurementData
{
    //public ArrayList<Double> avg_x =  new ArrayList<Double>();
    //public ArrayList<Double> avg_y =  new ArrayList<Double>();
    //public ArrayList<Double> avg_z =  new ArrayList<Double>();
    //public ArrayList<Double> avg_mx =  new ArrayList<Double>();
    //public ArrayList<Double> avg_my =  new ArrayList<Double>();
    //public ArrayList<Double> avg_mz =  new ArrayList<Double>();
    //public ArrayList<Double> avg_gx =  new ArrayList<Double>();
    //public ArrayList<Double> avg_gy =  new ArrayList<Double>();
    //public ArrayList<Double> avg_gz =  new ArrayList<Double>();
    public double avg_x = 0, avg_y = 0, avg_z = 0, avg_mx = 0, avg_my = 0, avg_mz = 0, avg_gx = 0, avg_gy = 0, avg_gz = 0;

    public int counter;


    public ArrayList<Long> timestamp = new ArrayList<Long>();
    public int steps = 0;
    public Boolean stepping = false;

    public MadgwickAHRS mMadgwickAHRS = new MadgwickAHRS(0.00481f, 0.000005f);

    public ArrayList<Float> rotAngle_Z = new ArrayList<>();
    public ArrayList<Float> rotAngle_Y = new ArrayList<>();
    public ArrayList<Float> rotAngle_X = new ArrayList<>();

    public ArrayList<Integer> currentStepCount = new ArrayList<>();

    public  ArrayList<Double> acc_x = new ArrayList<Double>();
    public  ArrayList<Double> acc_y = new ArrayList<Double>();
    public  ArrayList<Double> acc_z = new ArrayList<Double>();

    public  ArrayList<Double> gyr_x = new ArrayList<>();
    public  ArrayList<Double> gyr_y = new ArrayList<>();
    public  ArrayList<Double> gyr_z = new ArrayList<>();

    public  ArrayList<Double> mag_x = new ArrayList<>();
    public  ArrayList<Double> mag_y = new ArrayList<>();
    public  ArrayList<Double> mag_z = new ArrayList<>();

}
