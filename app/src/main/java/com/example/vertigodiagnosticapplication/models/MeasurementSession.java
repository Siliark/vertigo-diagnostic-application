package com.example.vertigodiagnosticapplication.models;
import java.net.Inet4Address;
import java.util.ArrayList;
import java.util.HashMap;

public class MeasurementSession {
    public String startTime;
    public Long testDuration;
    public String measuredVariables;
    public Double rotationAngle = 0.0;
    public Double stepCount = 0.0;
    public Integer sampleRate;
    public Boolean chest = false;
    public Boolean leftAnkle = false;
    public Boolean rightAnkle = false;

    public HashMap<String, MeasurementData> MeasurementData = new HashMap<>();
    public HashMap<String, CalibrationData> CalibrationData = new HashMap<>();
    public HashMap<String, MagnetometerCaliData> MagnetometerCaliData = new HashMap<>();
    public HashMap<String, GyroscopeCaliData> GyroscopeCaliData = new HashMap<>();

}
