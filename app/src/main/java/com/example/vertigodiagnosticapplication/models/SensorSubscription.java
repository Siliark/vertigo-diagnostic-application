package com.example.vertigodiagnosticapplication.models;

import android.util.Log;

import com.example.vertigodiagnosticapplication.controllers.AutomaticMeasurementSessionActivity;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.misc.VectorOperations;
import com.movesense.mds.MdsSubscription;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class SensorSubscription  {
    public int textSlower = 0;
    public double mag_x_bias;
    public double mag_y_bias;
    public double mag_z_bias;

    public  void unsubscribe() {
        for (MdsSubscription device : GVStore.SubscriptionMap.values())
            device.unsubscribe();
    }



    private  void calibrate(String packetSerial, MeasurementSession currentSession) {
        // Start Calibration
        // create acceleration arrays from the calibration data
        Double[] calibrationXList = currentSession.CalibrationData.get(packetSerial).acc_x.toArray(new Double[0]);
        Double[] calibrationYList = currentSession.CalibrationData.get(packetSerial).acc_y.toArray(new Double[0]);
        Double[] calibrationZList = currentSession.CalibrationData.get(packetSerial).acc_z.toArray(new Double[0]);

        // Calculate the Median
        double[] acc_medians = {VectorOperations.median(calibrationXList), VectorOperations.median(calibrationYList), VectorOperations.median(calibrationZList)};

        // Calculate the rotation Quaternion
        currentSession.CalibrationData.get(packetSerial).rotationQuaternion = VectorAlignment.calculateGravityAlignmentQuaternion(acc_medians);
    }

    private  void calibrateGyr(String packetSerial, MeasurementSession currentSession) {
        // Start Calibration

        // create gyroscope arrays from the calibration data
        Double[] gyrCalXList = currentSession.GyroscopeCaliData.get(packetSerial).gyr_x.toArray(new Double[0]);
        Double[] gyrCalYList = currentSession.GyroscopeCaliData.get(packetSerial).gyr_y.toArray(new Double[0]);
        Double[] gyrCalZList = currentSession.GyroscopeCaliData.get(packetSerial).gyr_z.toArray(new Double[0]);

        // Calculate the bias, which is the median.
        currentSession.GyroscopeCaliData.get(packetSerial).gyr_x_bias = VectorOperations.median(gyrCalXList);
        currentSession.GyroscopeCaliData.get(packetSerial).gyr_y_bias = VectorOperations.median(gyrCalYList);
        currentSession.GyroscopeCaliData.get(packetSerial).gyr_z_bias = VectorOperations.median(gyrCalZList);
    }

    private  double calibrateMag(Double[] valueArray) {
        // Start Calibration

        // Remove outliers
        // Creating the cleaned arrays
        ArrayList<Double> CleanedCaliList = new ArrayList<>();

        // Calculate mean
        double mean = VectorOperations.median(valueArray);
        // Calculate Sum
        double sum = 0.0;
        for (double number : valueArray) {
            sum += Math.pow(number - mean, 2);
        }
        //Calculate std
        double std = Math.sqrt(sum / valueArray.length);

        // Remove outliers
        for (Double aDouble : valueArray) {
            if (3 * std > Math.abs(aDouble - mean)) {
                CleanedCaliList.add(aDouble);
            }
        }
        // Calculate max and min
        double max = Collections.max(CleanedCaliList);
        double min = Collections.max(CleanedCaliList);
        // Return bias
        return (max + min) / 2;
    }



    public void gatherCalibrationData(MeasurementSession currentSession, DataResponse dataResponse, String packetSerial) {
        // If the packet contains data, feeds it to the calibrationData object
        for (int i = 0; i < dataResponse.body.acc_array.length; i++) {
            currentSession.CalibrationData.get(packetSerial).timestamp.add(dataResponse.body.timestamp);

            currentSession.CalibrationData.get(packetSerial).acc_x.add(dataResponse.body.acc_array[i].x);
            currentSession.CalibrationData.get(packetSerial).acc_y.add(dataResponse.body.acc_array[i].y);
            currentSession.CalibrationData.get(packetSerial).acc_z.add(dataResponse.body.acc_array[i].z);

            currentSession.CalibrationData.get(packetSerial).gyr_x.add(dataResponse.body.gyr_array[i].x);
            currentSession.CalibrationData.get(packetSerial).gyr_y.add(dataResponse.body.gyr_array[i].y);
            currentSession.CalibrationData.get(packetSerial).gyr_z.add(dataResponse.body.gyr_array[i].z);
        }

        // Calibrating the sensor when enough samples were gathered
        if (currentSession.CalibrationData.get(packetSerial).acc_x.size() == 1000) {
            calibrate(packetSerial, currentSession);
            GVStore.calibrationCount++; // Increment the calibration count that tells how many sensors were calibrated
        }
    }

    public void gatherGyroscopeCalibrationData(MeasurementSession currentSession, DataResponse dataResponse, String packetSerial) {
        // If the packet contains data, feeds it to the calibrationData object
        for (int i = 0; i < dataResponse.body.acc_array.length; i++) {
            currentSession.GyroscopeCaliData.get(packetSerial).timestamp.add(dataResponse.body.timestamp);

            currentSession.GyroscopeCaliData.get(packetSerial).gyr_x.add(dataResponse.body.gyr_array[i].x);
            currentSession.GyroscopeCaliData.get(packetSerial).gyr_y.add(dataResponse.body.gyr_array[i].y);
            currentSession.GyroscopeCaliData.get(packetSerial).gyr_z.add(dataResponse.body.gyr_array[i].z);
        }

        // Calibrating the sensor when enough samples were gathered
        if (currentSession.GyroscopeCaliData.get(packetSerial).gyr_x.size() == 1000) {
            calibrateGyr(packetSerial, currentSession);
            GVStore.gyrCalibrationCount++; // Increment the calibration count that tells how many sensors were calibrated
        }
    }

    public void gatherMagnetometerCalibrationData(MeasurementSession currentSession, DataResponse dataResponse, String packetSerial) {
        // If the packet contains data, feeds it to the calibrationData object
        for (int i = 0; i < dataResponse.body.acc_array.length; i++) {
            currentSession.MagnetometerCaliData.get(packetSerial).timestamp.add(dataResponse.body.timestamp);

            currentSession.MagnetometerCaliData.get(packetSerial).mag_x.add(dataResponse.body.gyr_array[i].x);
            currentSession.MagnetometerCaliData.get(packetSerial).mag_y.add(dataResponse.body.gyr_array[i].y);
            currentSession.MagnetometerCaliData.get(packetSerial).mag_z.add(dataResponse.body.gyr_array[i].z);
        }

        // Calibrating the sensor when enough samples were gathered
        if (currentSession.MagnetometerCaliData.get(packetSerial).mag_x.size() == 2000) {
            currentSession.MagnetometerCaliData.get(packetSerial).mag_x_bias = calibrateMag(currentSession.MagnetometerCaliData.get(packetSerial).mag_x.toArray(new Double[0]));
            currentSession.MagnetometerCaliData.get(packetSerial).mag_y_bias = calibrateMag(currentSession.MagnetometerCaliData.get(packetSerial).mag_y.toArray(new Double[0]));
            currentSession.MagnetometerCaliData.get(packetSerial).mag_z_bias = calibrateMag(currentSession.MagnetometerCaliData.get(packetSerial).mag_z.toArray(new Double[0]));

            GVStore.magCalibrationCount++; // Increment the calibration count that tells how many sensors were calibrated

            Log.e("mag",String.valueOf(currentSession.MagnetometerCaliData.get(packetSerial).mag_x_bias) );
            Log.e("mag",String.valueOf(currentSession.MagnetometerCaliData.get(packetSerial).mag_y_bias) );
            Log.e("mag",String.valueOf(currentSession.MagnetometerCaliData.get(packetSerial).mag_z_bias) );

        }
    }

    public void gatherMeasurementData(MeasurementSession currentSession, DataResponse dataResponse, String packetSerial){
        // If the packet contains data, feeds it to the measurementData object
        for (int i = 0; i < dataResponse.body.acc_array.length; i++) {


            // Rotate the vectors and remove bias
            double[] measAccVec = {dataResponse.body.acc_array[i].x, dataResponse.body.acc_array[i].y, dataResponse.body.acc_array[i].z};
            double[] rotAccVec = VectorAlignment.rotate(measAccVec, currentSession.CalibrationData.get(packetSerial).rotationQuaternion);
            double[] measGyrVec = {(dataResponse.body.gyr_array[i].x - currentSession.GyroscopeCaliData.get(packetSerial).gyr_x_bias) * 0.0174533, (dataResponse.body.gyr_array[i].y - currentSession.GyroscopeCaliData.get(packetSerial).gyr_y_bias) * 0.0174533, (dataResponse.body.gyr_array[i].z - currentSession.GyroscopeCaliData.get(packetSerial).gyr_z_bias) * 0.0174533};
            double[] rotGyrVec = VectorAlignment.rotate(measGyrVec, currentSession.CalibrationData.get(packetSerial).rotationQuaternion);
            double[] measMagVec = {dataResponse.body.mag_array[i].x - mag_x_bias, dataResponse.body.mag_array[i].y - mag_y_bias, dataResponse.body.mag_array[i].z - mag_z_bias};
            double[] rotMagVec = VectorAlignment.rotate(measMagVec, currentSession.CalibrationData.get(packetSerial).rotationQuaternion);

            // Add data to the measurement data list
            currentSession.MeasurementData.get(packetSerial).timestamp.add(dataResponse.body.timestamp);

            currentSession.MeasurementData.get(packetSerial).acc_x.add(rotAccVec[0]);
            currentSession.MeasurementData.get(packetSerial).acc_y.add(rotAccVec[1]);
            currentSession.MeasurementData.get(packetSerial).acc_z.add(rotAccVec[2]);
            currentSession.MeasurementData.get(packetSerial).gyr_x.add(rotGyrVec[0]);
            currentSession.MeasurementData.get(packetSerial).gyr_y.add(rotGyrVec[1]);
            currentSession.MeasurementData.get(packetSerial).gyr_z.add(rotGyrVec[2]);
            currentSession.MeasurementData.get(packetSerial).mag_x.add(rotMagVec[0]);
            currentSession.MeasurementData.get(packetSerial).mag_y.add(rotMagVec[1]);
            currentSession.MeasurementData.get(packetSerial).mag_z.add(rotMagVec[2]);




            // Averaging the data for the madgwick filter
            currentSession.MeasurementData.get(packetSerial).counter++;
            currentSession.MeasurementData.get(packetSerial).avg_x = currentSession.MeasurementData.get(packetSerial).avg_x + rotAccVec[0];
            currentSession.MeasurementData.get(packetSerial).avg_y = currentSession.MeasurementData.get(packetSerial).avg_y + rotAccVec[1];
            currentSession.MeasurementData.get(packetSerial).avg_z = currentSession.MeasurementData.get(packetSerial).avg_z + rotAccVec[2];

            currentSession.MeasurementData.get(packetSerial).avg_mx = currentSession.MeasurementData.get(packetSerial).avg_mx + rotMagVec[0];
            currentSession.MeasurementData.get(packetSerial).avg_my = currentSession.MeasurementData.get(packetSerial).avg_my + rotMagVec[1];
            currentSession.MeasurementData.get(packetSerial).avg_mz = currentSession.MeasurementData.get(packetSerial).avg_mz + rotMagVec[2];

            currentSession.MeasurementData.get(packetSerial).avg_gx = currentSession.MeasurementData.get(packetSerial).avg_gx + rotGyrVec[0];
            currentSession.MeasurementData.get(packetSerial).avg_gy = currentSession.MeasurementData.get(packetSerial).avg_gy + rotGyrVec[1];
            currentSession.MeasurementData.get(packetSerial).avg_gz = currentSession.MeasurementData.get(packetSerial).avg_gz + rotGyrVec[2];






            if(currentSession.MeasurementData.get(packetSerial).counter == 1) {
                currentSession.MeasurementData.get(packetSerial).mMadgwickAHRS.update(
                        (float)currentSession.MeasurementData.get(packetSerial).avg_gx/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_gy/1,

                        (float)currentSession.MeasurementData.get(packetSerial).avg_gz/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_x/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_y/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_z/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_mx/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_my/1,
                        (float)currentSession.MeasurementData.get(packetSerial).avg_mz/1
                );

                // Fetch the euler angles from the filter and add them to the measurementData object
                float[] eulerAngles = currentSession.MeasurementData.get(packetSerial).mMadgwickAHRS.getEulerAngles();
                currentSession.MeasurementData.get(packetSerial).rotAngle_Z.add(eulerAngles[0]);
                currentSession.MeasurementData.get(packetSerial).rotAngle_Y.add(eulerAngles[1]);
                currentSession.MeasurementData.get(packetSerial).rotAngle_X.add(eulerAngles[2]);


                //  Calculate Steps
                if(currentSession.leftAnkle && currentSession.rightAnkle){
                    Log.e("Calculating  steps", "gatherMeasurementData: ");
                    calculateSteps(currentSession, packetSerial, eulerAngles);
                }
                // Reset the counter and avg values
                currentSession.MeasurementData.get(packetSerial).counter = 0;
                currentSession.MeasurementData.get(packetSerial).avg_x = 0;
                currentSession.MeasurementData.get(packetSerial).avg_y = 0;
                currentSession.MeasurementData.get(packetSerial).avg_z = 0;

                currentSession.MeasurementData.get(packetSerial).avg_mx = 0;
                currentSession.MeasurementData.get(packetSerial).avg_my = 0;
                currentSession.MeasurementData.get(packetSerial).avg_mz = 0;

                currentSession.MeasurementData.get(packetSerial).avg_gx = 0;
                currentSession.MeasurementData.get(packetSerial).avg_gy = 0;
                currentSession.MeasurementData.get(packetSerial).avg_gz = 0;
          } else {
                // In case not 52 samples were received the previous angle and step count number is added to the data
                float[] eulerAngles = currentSession.MeasurementData.get(packetSerial).mMadgwickAHRS.getEulerAngles();
                currentSession.MeasurementData.get(packetSerial).rotAngle_Z.add(eulerAngles[0]);
                currentSession.MeasurementData.get(packetSerial).rotAngle_Y.add(eulerAngles[1]);
                currentSession.MeasurementData.get(packetSerial).rotAngle_X.add(eulerAngles[2]);
                currentSession.MeasurementData.get(packetSerial).currentStepCount.add(currentSession.MeasurementData.get(packetSerial).steps);
            }
        }
    }



    private  void calculateSteps(MeasurementSession currentSession, String packetSerial, float[] eulerAngles){
        // Calculate angle
        double stepAngle = Math.sqrt(Math.pow(eulerAngles[2],2) + Math.pow(eulerAngles[1],2));
        Log.e("eulerAngles1", String.valueOf(eulerAngles[1]));
        Log.e("eulerAngles2", String.valueOf(eulerAngles[2]));
        Log.e("stepAngle", String.valueOf(stepAngle));


        //  Calculating step count ()
        if(currentSession.MeasurementData.get(packetSerial).stepping && stepAngle<30){
            currentSession.MeasurementData.get(packetSerial).stepping = false;
            Log.e("sensorStep False: ", packetSerial );
        } else if(!currentSession.MeasurementData.get(packetSerial).stepping &&  stepAngle>30)
        {
            currentSession.MeasurementData.get(packetSerial).stepping =true;
            currentSession.MeasurementData.get(packetSerial).steps++;
            currentSession.stepCount++;
            Log.e("sensorStep True:", packetSerial );
        } else {
            Log.e("else", String.valueOf(stepAngle));
        }
        // Add the steps for the row
        currentSession.MeasurementData.get(packetSerial).currentStepCount.add(currentSession.MeasurementData.get(packetSerial).steps);
    }


}

