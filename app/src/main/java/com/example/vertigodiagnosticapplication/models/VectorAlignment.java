package com.example.vertigodiagnosticapplication.models;

import android.util.Log;

import com.example.vertigodiagnosticapplication.misc.VectorOperations;

// This class contains the methods needed to calibrate the sensors
public class VectorAlignment {


    // Method that produces the Rotation Matrix or Quaternion necessary to align the Z-axis with gravity
    public static double[] calculateGravityAlignmentQuaternion(double[] vector){
        double[] alignmentQuaternion = new double[4];

        // Calculating the median of the samples
        //double[] medianAccVec = {VectorOperations.median(xAxis), VectorOperations.median(yAxis), VectorOperations.median(zAxis)};
        // Normalize the median acceleration vector.
        double[] normalizedVector = VectorOperations.normalize(vector);
        // Check if parallel
        double[] gravityVector = {0,0,1};
        String isParallel = VectorOperations.checkIfParallel(normalizedVector, gravityVector);

        // Create the alignment quaternion
        if(isParallel.equals("False")){

            double[] cross_product = VectorOperations.crossProduct(normalizedVector, gravityVector);

            alignmentQuaternion[0] = 1 + VectorOperations.dotProduct(normalizedVector, gravityVector);
            alignmentQuaternion[1] = cross_product[0];
            alignmentQuaternion[2] = cross_product[1];
            alignmentQuaternion[3] = cross_product[2];

        } else if (isParallel.equals("Opposite")){
            double x = 1;
            double y = 1;
            double z;
            z = - (normalizedVector[0]*x+normalizedVector[1]*y) / normalizedVector[2];
            // Calculating the rotation quaternion based on the perpendicular axis. The real part, being 0, induces a rotation of 180 degrees around that axis, hence "fliping" the vector
            alignmentQuaternion[0] = 0;
            alignmentQuaternion[1] = x;
            alignmentQuaternion[2] = y;
            alignmentQuaternion[3] = z;
        } else {
            // Unit Quaternion that does not create any rotation
            alignmentQuaternion[0] = 1;
            alignmentQuaternion[1] = 0;
            alignmentQuaternion[2] = 0;
            alignmentQuaternion[3] = 0;
        }

        return  VectorOperations.normalize(alignmentQuaternion);
    }



    // Method to rotate a vector with a given alignment quaternion
    public static double[] rotate(double[] measurementVector, double[] alignmentQuaternion){
        double magnitude = Math.sqrt(measurementVector[0] * measurementVector[0] * measurementVector[1] * measurementVector[1] * measurementVector[2] * measurementVector[2]);
        double [] measurementVectorQuaternion = {0, measurementVector[0], measurementVector[1], measurementVector[2]};//VectorOperations.normalize(new double[]{0, measurementVector[0], measurementVector[1], measurementVector[2]});
        double [] alignmentQuaternionConjugate = VectorOperations.getConjugate(alignmentQuaternion);

        double[] rotatedVectorQuaternion = VectorOperations.quaternionMultiplication(VectorOperations.quaternionMultiplication(alignmentQuaternion, measurementVectorQuaternion), alignmentQuaternionConjugate);

        return new double[]{rotatedVectorQuaternion[1], rotatedVectorQuaternion[2], rotatedVectorQuaternion[3]};
    }



}
