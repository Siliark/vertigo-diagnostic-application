package com.example.vertigodiagnosticapplication.models;

import java.util.ArrayList;

public class GyroscopeCaliData {

        public double gyr_x_bias, gyr_y_bias, gyr_z_bias;


        public ArrayList<Long> timestamp = new ArrayList<>();

        public  ArrayList<Double> gyr_x = new ArrayList<>();
        public  ArrayList<Double> gyr_y = new ArrayList<>();
        public  ArrayList<Double> gyr_z = new ArrayList<>();
}


