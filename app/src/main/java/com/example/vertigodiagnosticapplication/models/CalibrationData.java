package com.example.vertigodiagnosticapplication.models;

import java.util.ArrayList;

public class CalibrationData {

    public double[] rotationQuaternion = new double[4];

    public ArrayList<Long> timestamp = new ArrayList<Long>();

    public  ArrayList<Double> acc_x = new ArrayList<Double>();
    public  ArrayList<Double> acc_y = new ArrayList<Double>();
    public  ArrayList<Double> acc_z = new ArrayList<Double>();

    public  ArrayList<Double> gyr_x = new ArrayList<>();
    public  ArrayList<Double> gyr_y = new ArrayList<>();
    public  ArrayList<Double> gyr_z = new ArrayList<>();

}
