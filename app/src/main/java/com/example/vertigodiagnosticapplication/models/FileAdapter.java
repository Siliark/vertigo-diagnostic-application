package com.example.vertigodiagnosticapplication.models;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.controllers.SessionAnalysisActivity;

import java.io.File;


public class FileAdapter extends RecyclerView.Adapter<FileAdapter.ViewHolder> {

    Context context;
    File[] fileList;

    public FileAdapter(Context context, File[] fileList){
        this.context = context;
        this.fileList = fileList;
    }


    @Override
    public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(context).inflate(R.layout.item_savedsession, parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        File currentFile = fileList[position];
        holder.fileName.setText(" "+currentFile.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {

            @Override
            public  void onClick(View v){

                Intent intent = new Intent(context, SessionAnalysisActivity.class);
                String FilePath = currentFile.getAbsolutePath();
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("filePath",FilePath);
                intent.putExtra("source", "Files");
                context.startActivity(intent);
            }


        });


    }

    @Override
    public int getItemCount() {
        return fileList.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView fileName;

        public ViewHolder(View itemView){
            super(itemView);
            fileName = itemView.findViewById(R.id.fileName);
        }

    }

}
