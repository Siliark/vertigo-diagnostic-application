package com.example.vertigodiagnosticapplication.models;

import java.util.ArrayList;

public class MagnetometerCaliData {

        public double mag_x_bias, mag_y_bias, mag_z_bias;


        public ArrayList<Long> timestamp = new ArrayList<>();

        public  ArrayList<Double> mag_x = new ArrayList<>();
        public  ArrayList<Double> mag_y = new ArrayList<>();
        public  ArrayList<Double> mag_z = new ArrayList<>();
}


