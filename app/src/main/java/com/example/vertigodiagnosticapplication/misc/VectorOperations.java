package com.example.vertigodiagnosticapplication.misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VectorOperations {

    // Helping methods

    // Method to calculate the median of a array
    public static double median(Double[] array) {
        // sort array
        Arrays.sort(array);
        double median;
        // get count of scores
        int totalElements = array.length;
        // check if total number of scores is even
        if (totalElements % 2 == 0) {
            double sumOfMiddleElements = array[totalElements / 2] +
                    array[totalElements / 2 - 1];
            // calculate average of middle elements
            median = ((double) sumOfMiddleElements) / 2;
        } else {
            // get the middle element
            median = (double) array[array.length / 2];
        }
        return median;
    }


    // Method to normalise a vector
    public static double[] normalize(double[] vector){
        double magnitude = 0;
        double[] normalizedVector = new double[vector.length];
        // this is an enhanced loop that loop through the elements without caring for the index
        // Calculating the magnitude needed to normalize
        for (double v : vector) {
            magnitude += Math.pow(v, 2);
        }
        magnitude = Math.sqrt(magnitude);

        // Normalizing
        for(int i = 0; i<vector.length; i++ ){
            normalizedVector[i] = vector[i]/magnitude;
        }
        return normalizedVector;
    }


    // Method to check if two vectors are parallel
    public static String checkIfParallel(double[] measuredVec, double[] referenceVec){
        String isParallel;

        double dotProduct = dotProduct(measuredVec,referenceVec);

        if(dotProduct < -0.99999){
            isParallel = "Opposite";
        } else if (dotProduct > 0.99999){
            isParallel = "Same";
        } else {
            isParallel = "False";
        }
        return isParallel;
    }


    // Method to calculate the cross product  between two vectors
    public static double[] crossProduct(double[] vector_A, double[] vector_B)
    {
        double[] cross_P = new double[vector_A.length];

        cross_P[0] = vector_A[1] * vector_B[2]
                - vector_A[2] * vector_B[1];
        cross_P[1] = vector_A[2] * vector_B[0]
                - vector_A[0] * vector_B[2];
        cross_P[2] = vector_A[0] * vector_B[1]
                - vector_A[1] * vector_B[0];

        return cross_P;
    }


    public static double dotProduct(double[] vector_A, double[] vector_B)
    {
        double dotProduct = 0.0;
        for (int i = 0; i < vector_A.length; i++){
            dotProduct += vector_A[i] * vector_B[i];
        }

        return dotProduct;
    }






    // quaternion operations
    public static double[] getConjugate(double[] quaternion) {
        double[] conjugate = {quaternion[0], -1 * quaternion[1], -1 * quaternion[2], -1 * quaternion[3]};
        return conjugate;
    }


    // Remember the order matters with quaternion multiplication (I think)
    public static double[] quaternionMultiplication(double[] r, double[] q) {

        // Components of the product.
        final double w = r[0]*q[0]-r[1]*q[1]-r[2]*q[2]-r[3]*q[3];
        final double x = r[0]*q[1]+r[1]*q[0]+r[2]*q[3]-r[3]*q[2];
        final double y = r[0]*q[2]-r[1]*q[3]+r[2]*q[0]+r[3]*q[1];
        final double z = r[0]*q[3]+r[1]*q[2]-r[2]*q[1]+r[3]*q[0];

        // resulting quaternion
        return new double[]{w, x, y, z};
    }
}
