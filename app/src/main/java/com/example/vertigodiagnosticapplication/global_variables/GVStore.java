package com.example.vertigodiagnosticapplication.global_variables;

import android.app.Application;

import com.example.vertigodiagnosticapplication.models.MeasurementData;
import com.example.vertigodiagnosticapplication.models.MeasurementSession;
import com.movesense.mds.Mds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


import com.example.vertigodiagnosticapplication.utils.BluetoothStatusMonitor;
import com.example.vertigodiagnosticapplication.utils.RxBle;
import com.movesense.mds.MdsSubscription;
import com.polidea.rxandroidble2.RxBleDevice;


public class GVStore extends Application{

    // MDS object used to connect, subscribe, etc.. (Should be changed as it might be a memory leak)
    public static Mds mds_obj;

    // For all these dictionaries the URI of the sensors are used as a key. This is done because it is visible on the device making it easier to know which sensor is used
    //Stores the allocated placement of the sensors
    public static HashMap<String, String> SensorPlacement = new HashMap<>();
    // Stores the Sensor RxBleDevice object
    public static HashMap<String, RxBleDevice> SensorMap = new HashMap<>();
    // Stores the subscription objects for each sensor
    public static HashMap<String, MdsSubscription> SubscriptionMap = new HashMap<>();
    // Stores the Measurement Sessions with the creation time as a key
    public static HashMap<String, MeasurementSession> MeasurementSessionMap = new HashMap<>();


    // Variables used to control the calibration and measurement
    public static Boolean CalActive = false;
    public static Boolean MeasActive = false;
    public static Boolean MagActive = false;
    public static Boolean GyrActive = false;

    public static Boolean CalDone = false;
    public static Boolean MeasDone = false;
    public static Boolean MagDone = false;
    public static Boolean GyrDone = false;

    //Variable to check if all sensors are calibrated
    public static int calibrationCount = 0;
    public static int magCalibrationCount = 0;
    public static int gyrCalibrationCount = 0;

    // Variable to check if all selected devices are connected
    public static int connectingCount = 0;
    public static int subscriptionCount = 0;



    // Name of the folder where sessions are saved
    public static String SavedSessionsFolder = "SavedSessions";



    @Override
    public void onCreate() {
        super.onCreate();
        // Initialize RxBleWrapper
        RxBle.Instance.initialize(this);

        SensorPlacement.put("leftAnkle", "Unassigned");
        SensorPlacement.put("rightAnkle", "Unassigned");
        SensorPlacement.put("chest", "Unassigned");

        BluetoothStatusMonitor.INSTANCE.initBluetoothStatus();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
