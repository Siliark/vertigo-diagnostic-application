package com.example.vertigodiagnosticapplication.controllers;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.chaquo.python.PyObject;
import com.chaquo.python.Python;
import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.CalibrationData;
import com.example.vertigodiagnosticapplication.models.GraphNameAdapter;
import com.example.vertigodiagnosticapplication.models.MeasurementData;
import com.example.vertigodiagnosticapplication.models.MeasurementSession;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;
import java.util.stream.Collectors;


public class SessionAnalysisActivity extends AppCompatActivity {

    //File SavedSession;
    MeasurementSession ParsedSession = new MeasurementSession();
    HashMap<String, String> SensorLocations = new HashMap<>();
    //ArrayList for URIs so that they are kept in order.
    ArrayList<String> sensorURIs = new ArrayList<String>();
    String sessionSetting;
    ArrayList<String> possibleGraphs;
    ListView graphList;
    LinearLayout ParsedSessionInfo;
    ImageView GraphingPicture;
    String currentGraph = "";

    Python PythonInstance;
    PyObject GraphingScripts;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_session_analysis);
        String FilePath = getIntent().getStringExtra("filePath");

        PythonInstance = Python.getInstance();
        GraphingScripts = PythonInstance.getModule("GraphingScripts");
        String source = getIntent().getStringExtra("source");
        if(source.equals("Files")){
            parseSessionData(FilePath);
        } else if(source.equals("Results")){
            ParsedSession = GVStore.MeasurementSessionMap.get(getIntent().getStringExtra("currentSessionID"));
            SensorLocations.put("position_rightAnkle", GVStore.SensorPlacement.get("rightAnkle"));
            SensorLocations.put("position_leftAnkle", GVStore.SensorPlacement.get("leftAnkle"));
            SensorLocations.put("position_chest", GVStore.SensorPlacement.get("chest"));
        }

        writeSessionInfo();
        possibleGraphs = definePossibleGraphs();
        ParsedSessionInfo = findViewById(R.id.ParsedSensorInfo);
        GraphingPicture = findViewById(R.id.GraphPicture);
        graphList = findViewById(R.id.listOfGraphs);
        //ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,possibleGraphs);
        GraphNameAdapter arrayAdapter = new GraphNameAdapter(this,possibleGraphs);
        graphList.setAdapter(arrayAdapter);

        graphList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                createGraph(possibleGraphs.get(i));
            }
        });

    }


    // Currently there is only one session type, with fixed csv template.
    // Therefore the parser is semi hardcoded.
    // TODO: add switch case that selects correct parser if there are multiple csv templates for different measurement sessions.
    // Parses through the files data.
    private void parseSessionData(String filePath){
        final File csvFile = new File(filePath);

        try {
            FileInputStream iStream = new FileInputStream(csvFile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(iStream));
            String line;
            String[] splitLine;

            // Read the First Header and its data
            // Line 1.
            line = reader.readLine();
            splitLine = line.split(",");
            int firstSensorIndex = Arrays.asList(splitLine).indexOf("sensor_1");
            int stepCountIndex = Arrays.asList(splitLine).indexOf("steps");
            int rotAngleIndex = Arrays.asList(splitLine).indexOf("angle_of_rotation");
            int sessionSettingIndex = Arrays.asList(splitLine).indexOf("measured_variables");

            // Line 2.
            line = reader.readLine();
            splitLine = line.split(",");
            ParsedSession.stepCount = Double.parseDouble(splitLine[stepCountIndex]);
            ParsedSession.rotationAngle = Double.parseDouble(splitLine[rotAngleIndex]);
            ParsedSession.measuredVariables = splitLine[sessionSettingIndex];
            for (int j = firstSensorIndex; j < splitLine.length;j++){
                sensorURIs.add(splitLine[j]);
                ParsedSession.MeasurementData.put(splitLine[j],new MeasurementData());
                ParsedSession.CalibrationData.put(splitLine[j],new CalibrationData());
            }

            // Line 3.
            line = reader.readLine();
            splitLine = line.split(",");
            for (int j = firstSensorIndex; j < splitLine.length;j++){
                SensorLocations.put(splitLine[j],sensorURIs.get(j-5));
                switch(splitLine[j]){
                    case "position_leftAnkle":
                        ParsedSession.leftAnkle = true;
                        break;
                    case "position_rightAnkle":
                        ParsedSession.rightAnkle = true;
                        break;
                    case "position_chest":
                        ParsedSession.chest = true;
                        break;
                    default:
                        Log.e("ParseError:","Unknown sensor position in parseSessionDataFunction");
                        //TODO: Add error handling
                }
            }

            // Line 4.
            line = reader.readLine();
            splitLine = line.split(",");

            // The sample rate is in form "sampleRate_X"
            String sampleRate[] = splitLine[firstSensorIndex].split("_");
            ParsedSession.sampleRate = Integer.valueOf(sampleRate[1]);


            // Line 5. Should be empty as it should be separator before measurement data headers
            line = reader.readLine();
            if(!line.isEmpty()){
                Toast.makeText(this, "Something went wrong with line reading. Check 1", Toast.LENGTH_LONG).show();
                finish();
            }


            // Measurement Data should start here
            // Measurement data headers. Since template is fixed the headers are just skipped.
            line = reader.readLine();

            // Parse Through Measurement Data until empty line is reached which is the divider between measurement data and calibration data.
            int headersPerSensor = 14;
            line = reader.readLine();
            while(!line.isEmpty()){
                splitLine = line.split(",");
                for (int i = 0; i < sensorURIs.size();i++){
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).timestamp.add(Long.parseLong(splitLine[0+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).currentStepCount.add(Integer.parseInt(splitLine[1+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).rotAngle_X.add(Float.parseFloat(splitLine[2+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).rotAngle_Y.add(Float.parseFloat(splitLine[3+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).rotAngle_Z.add(Float.parseFloat(splitLine[4+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).acc_x.add(Double.parseDouble(splitLine[5+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).acc_y.add(Double.parseDouble(splitLine[6+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).acc_z.add(Double.parseDouble(splitLine[7+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).gyr_x.add(Double.parseDouble(splitLine[8+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).gyr_y.add(Double.parseDouble(splitLine[9+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).gyr_z.add(Double.parseDouble(splitLine[10+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).mag_x.add(Double.parseDouble(splitLine[11+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).mag_y.add(Double.parseDouble(splitLine[12+i*headersPerSensor]));
                    ParsedSession.MeasurementData.get(sensorURIs.get(i)).mag_z.add(Double.parseDouble(splitLine[13+i*headersPerSensor]));
                }
                line = reader.readLine();
            }

            // Calibration Data should start here
            // Calibration Data Headers. Since template is fixed the headers are skipped
            line = reader.readLine();

            headersPerSensor = 7;
            line = reader.readLine();

            while(line != null){
                splitLine = line.split(",");
                for (int i = 0; i < sensorURIs.size();i++){
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).timestamp.add(Long.parseLong(splitLine[0+i*headersPerSensor]));
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).acc_x.add(Double.parseDouble(splitLine[1+i*headersPerSensor]));
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).acc_y.add(Double.parseDouble(splitLine[2+i*headersPerSensor]));
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).acc_z.add(Double.parseDouble(splitLine[3+i*headersPerSensor]));
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).gyr_x.add(Double.parseDouble(splitLine[4+i*headersPerSensor]));
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).gyr_y.add(Double.parseDouble(splitLine[5+i*headersPerSensor]));
                    ParsedSession.CalibrationData.get(sensorURIs.get(i)).gyr_z.add(Double.parseDouble(splitLine[6+i*headersPerSensor]));
                }
                line = reader.readLine();
            }

        }
        catch (IOException e) {
            Toast.makeText(this, "Something went wrong with file reading", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    private void writeSessionInfo(){

        TextView sessionInfo = findViewById(R.id.SessionInfoText);
        StringBuilder InfoString = new StringBuilder();

        //Add Position Info
        InfoString.append("Sensor locations:");
        Set<String> locations = SensorLocations.keySet();
        boolean commaCheck = false;
        if(ParsedSession.chest){
            if(commaCheck){
                InfoString.append(",");
            }
            InfoString.append(" Chest");
            commaCheck = true;
        }
        if(ParsedSession.leftAnkle){
            if(commaCheck){
                InfoString.append(",");
            }
            InfoString.append(" Left Ankle");
            commaCheck = true;
        }
        if(ParsedSession.rightAnkle){
            if(commaCheck){
                InfoString.append(",");
            }
            InfoString.append(" Right Ankle");
            commaCheck = true;
        }
        InfoString.append("\n\n");

        InfoString.append("Measured Variables: ").append(ParsedSession.measuredVariables).append("\n\n");
        InfoString.append("Total steps: ").append(ParsedSession.stepCount).append("\n\n");
        InfoString.append("Rotation angle: ").append(ParsedSession.rotationAngle).append("\n\n");
        //InfoString.append("Number of data points: ").append(ParsedSession.MeasurementData.get(sensorURIs.get(0)).acc_x.size()).append("\n\n");

        sessionInfo.setText(InfoString);
    }

    // Goes through the parsed session and creates a list of all possible graphs that can be done with the data
    private ArrayList<String> definePossibleGraphs() {
        ArrayList<String> graphNames = new ArrayList<String>();

        if(ParsedSession.chest){
            //graphNames.add("Chest rotation/Time");
            graphNames.add("Swaying");
        }

        if(ParsedSession.leftAnkle){
            graphNames.add("Left Ankle Steps/Time");
            graphNames.add("Left Ankle Rotation/Steps");
            graphNames.add("Left Ankle Rotation/Time");
        }

        if(ParsedSession.rightAnkle){
            graphNames.add("Right Ankle Steps/Time");
            graphNames.add("Right Ankle Rotation/Steps");
            graphNames.add("Right Ankle Rotation/Time");
        }

        if(ParsedSession.rightAnkle && ParsedSession.leftAnkle){
            //graphNames.add("Total Step Count/time");
            //graphNames.add("Total Rotation/Steps");
            //graphNames.add("Total Rotation/Time");
        }

        return graphNames;
    }

    // Call the wanted graph generation function in the case statement
    private void createGraph(String graphName){

        // If same graph is selected again, the graph will be removed.
        if(currentGraph == graphName){
            GraphingPicture.setVisibility(View.INVISIBLE);
            ParsedSessionInfo.setVisibility(View.VISIBLE);
            currentGraph = "";
            return;
        }

        currentGraph = graphName;

        switch(graphName){
            case "Right Ankle Steps/Time":
            {
                ArrayList<Long> timeData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).timestamp;
                ArrayList<Integer> stepData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).currentStepCount;
                String timeDataString = timeData.stream().map(Object::toString).collect(Collectors.joining(","));
                String stepDataString = stepData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("right_ankle_steps_time",stepDataString,timeDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;
            }
            case "Right Ankle Rotation/Time":
            {
                ArrayList<Long> timeData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).timestamp;
                ArrayList<Float> rotXData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).rotAngle_X;
                ArrayList<Float> rotYData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).rotAngle_Y;
                ArrayList<Float> rotZData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).rotAngle_Z;
                String timeDataString = timeData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotXDataString = rotXData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotYDataString = rotYData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotZDataString = rotZData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("right_ankle_rotations_time",rotXDataString,rotYDataString,rotZDataString,timeDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;
            }
            case "Left Ankle Steps/Time":
            {
                ArrayList<Long> timeData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).timestamp;
                ArrayList<Integer> stepData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).currentStepCount;
                String timeDataString = timeData.stream().map(Object::toString).collect(Collectors.joining(","));
                String stepDataString = stepData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("left_ankle_steps_time",stepDataString,timeDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;

            }
            case "Left Ankle Rotation/Time":
            {
                ArrayList<Long> timeData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).timestamp;
                ArrayList<Float> rotXData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).rotAngle_X;
                ArrayList<Float> rotYData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).rotAngle_Y;
                ArrayList<Float> rotZData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).rotAngle_Z;
                String timeDataString = timeData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotXDataString = rotXData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotYDataString = rotYData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotZDataString = rotZData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("left_ankle_rotations_time",rotXDataString,rotYDataString,rotZDataString,timeDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;
            }
            case "Left Ankle Rotation/Steps":
            {
                ArrayList<Integer> stepData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).currentStepCount;
                ArrayList<Float> rotXData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).rotAngle_X;
                ArrayList<Float> rotYData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).rotAngle_Y;
                ArrayList<Float> rotZData = ParsedSession.MeasurementData.get(SensorLocations.get("position_leftAnkle")).rotAngle_Z;
                String stepDataString = stepData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotXDataString = rotXData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotYDataString = rotYData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotZDataString = rotZData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("left_ankle_rotations_steps",rotXDataString,rotYDataString,rotZDataString,stepDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;
            }
            case "Right Ankle Rotation/Steps":
            {
                ArrayList<Integer> stepData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).currentStepCount;
                ArrayList<Float> rotXData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).rotAngle_X;
                ArrayList<Float> rotYData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).rotAngle_Y;
                ArrayList<Float> rotZData = ParsedSession.MeasurementData.get(SensorLocations.get("position_rightAnkle")).rotAngle_Z;
                String stepDataString = stepData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotXDataString = rotXData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotYDataString = rotYData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotZDataString = rotZData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("right_ankle_rotations_steps",rotXDataString,rotYDataString,rotZDataString,stepDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;
            }
            case "Swaying":
            {
                ArrayList<Float> rotXData = ParsedSession.MeasurementData.get(SensorLocations.get("position_chest")).rotAngle_X;
                ArrayList<Float> rotYData = ParsedSession.MeasurementData.get(SensorLocations.get("position_chest")).rotAngle_Y;
                String rotXDataString = rotXData.stream().map(Object::toString).collect(Collectors.joining(","));
                String rotYDataString = rotYData.stream().map(Object::toString).collect(Collectors.joining(","));

                PyObject graph = GraphingScripts.callAttr("swaying_over_time",rotXDataString,rotYDataString);
                String graphString = graph.toString();
                byte graphData[] = android.util.Base64.decode(graphString, Base64.DEFAULT);
                Bitmap GraphBitmap = BitmapFactory.decodeByteArray(graphData,0,graphData.length);
                GraphingPicture.setImageBitmap(GraphBitmap);
                break;
            }
            default:
                Toast.makeText(this, "Selected graph isn't programmed", Toast.LENGTH_SHORT).show();
                break;


        }
        ParsedSessionInfo.setVisibility(View.INVISIBLE);
        GraphingPicture.setVisibility(View.VISIBLE);
    }

}
