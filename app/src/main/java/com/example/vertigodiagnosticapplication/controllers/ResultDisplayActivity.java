package com.example.vertigodiagnosticapplication.controllers;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;
import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.MeasurementSession;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class ResultDisplayActivity extends AppCompatActivity {
    ArrayList<String> listDataArray;
    private TextView angle, steps, duration;
    MeasurementSession currentSession;
    ArrayList<String> dataKeys;
    ArrayList<String> calibrationKeys;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_display);
        currentSession = GVStore.MeasurementSessionMap.get(getIntent().getStringExtra("currentSessionID"));
        //Store the keys to ArrayList just in case because hashmap/set doesn't maintain  order
        dataKeys = new ArrayList<String>(currentSession.MeasurementData.keySet());
        calibrationKeys = new ArrayList<String>(currentSession.CalibrationData.keySet());
        // Step Count function
        angle = (TextView) findViewById(R.id.angle);
        steps = (TextView) findViewById(R.id.steps);
        duration = (TextView) findViewById(R.id.duration);

        angle.setText(String.format("%.2f", currentSession.rotationAngle));
        steps.setText(String.format("%.2f", currentSession.stepCount));
        duration.setText(String.valueOf(currentSession.testDuration));


        // Angle measurement function

        // save button that uses a save model to save the data to a csv (google doc, etc)
        // On succesful save goes back to the main activity
        // https://stackoverflow.com/questions/60059180/how-to-save-listview-items-to-a-csv-file

        // retake measurement button -> goes back to the measurement setup activity

    }


    public void processCSV(View view) {

        try {

            boolean writePermissionStatus = checkStoragePermission(false);
            //Check for permission
            if (!writePermissionStatus) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                return;
            } else {
                boolean writePermissionStatusAgain = checkStoragePermission(true);
                if (!writePermissionStatusAgain) {
                    Toast.makeText(this, "Permission not granted", Toast.LENGTH_LONG).show();
                    return;
                } else {
                    //Permission Granted. Export
                    exportDataToCSV();

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void displayGraphs(View view) {
        // Start python (Chaquopy) if its not started yet
        if (! Python.isStarted()) {
            Python.start(new AndroidPlatform(this));
        }

        Intent graphPlotIntent = new Intent(ResultDisplayActivity.this, SessionAnalysisActivity.class);
        graphPlotIntent.putExtra("currentSessionID",getIntent().getStringExtra("currentSessionID"));
        graphPlotIntent.putExtra("source","Results");
        graphPlotIntent.putExtra("filePath","None");

        startActivity(graphPlotIntent); // Switch Activity
    }

    private String infoToCSV(){
        StringBuilder infoString = new StringBuilder();

        // Add Info Headers
        infoString.append("session_start,session_stop,steps,angle_of_rotation,measured_variables");
        for (int i = 0; i < dataKeys.size();i++){
            infoString.append(",sensor_").append(i+1);
        }
        infoString.append("\n");

        // Add Info Data
        for(int i = 0; i<3; i++){
            switch (i) {
                case 0:
                    infoString.append("session_start_value,session_stop_value,").append(currentSession.stepCount).append(",").append(currentSession.rotationAngle).append(",IMU9");
                    for (int j = 0; j < dataKeys.size();j++){
                        //infoString.append(",uri_").append(j+1);
                        infoString.append(",uri_").append(dataKeys.get(j));
                    }
                    infoString.append("\n");
                    break;
                case 1:
                    infoString.append(",,,,");
                    for (int j = 0; j < dataKeys.size();j++){
                        //infoString.append(",position_").append(j+1);
                        for(String position : GVStore.SensorPlacement.keySet()){
                            if(GVStore.SensorPlacement.get(position) == dataKeys.get(j)){
                                infoString.append(",position_").append(position);
                                Log.i("TestiPlacement","teksti: "+position);
                            }
                        }
                    }
                    infoString.append("\n");
                    break;
                case 2:
                    infoString.append(",,,,");
                    for (int j = 0; j < dataKeys.size();j++){
                        //infoString.append(",sample_rate_").append(j+1);
                        infoString.append(",sampleRate_").append(currentSession.sampleRate);
                    }
                    infoString.append("\n");
                    break;
            }
        }
        infoString.append("\n");
        return infoString.toString();
    }


    private String measToCSV(){
        StringBuilder measString = new StringBuilder();

        // Note that currently the smallest data array size defines how many data points are saved for each sensor.
        // That is, if sensor A has measured 400 data points and sensor B only 398, only 398 data points will be saved for each sensor.
        // This means that there can be some data loss when the measurement and calibration data are saved.
        // However for purposes of this project, the effects of these data losses are presumed to be meaningless.
        int max_sample_nbr = currentSession.MeasurementData.get(dataKeys.get(0)).acc_x.size();

        // Add Measurement Headers
        for(int i = 0; i<dataKeys.size(); i++){
            // Get sample size to prevent new loop. Use keys to get data so that the order will be kept the same
            if(max_sample_nbr > currentSession.MeasurementData.get(dataKeys.get(i)).acc_x.size()){
                max_sample_nbr = currentSession.MeasurementData.get(dataKeys.get(i)).acc_x.size();
            }
            // Timestamp
            measString.append("timestamp_").append(i).append(",");
            // Step count
            measString.append("step_count_").append(i).append(",");
            // Rotation
            measString.append("rotAngle_x_").append(i).append(",");
            measString.append("rotAngle_y_").append(i).append(",");
            measString.append("rotAngle_z_").append(i).append(",");
            // Accelerometer
            measString.append("acc_x_").append(i).append(",");
            measString.append("acc_y_").append(i).append(",");
            measString.append("acc_z_").append(i).append(",");
            // Gyroscope
            measString.append("gyr_x_").append(i).append(",");
            measString.append("gyr_y_").append(i).append(",");
            measString.append("gyr_z_").append(i).append(",");
            // Magnetometer
            measString.append("mag_x_").append(i).append(",");
            measString.append("mag_y_").append(i).append(",");
            measString.append("mag_z_").append(i);

            if(i < dataKeys.size()-1){
                measString.append(",");
            } else {
                measString.append("\n");
            }
        }


        // TODO: Currently there is a bug that if you use only right ankle sensor, the currentStepCount variable will have less data than other arrays.
        //      Therefore here we add the lastest stepcount value until it is as long as other arrays
        if(currentSession.MeasurementData.get(dataKeys.get(0)).currentStepCount.size() < currentSession.MeasurementData.get(dataKeys.get(0)).acc_x.size()){
            while(currentSession.MeasurementData.get(dataKeys.get(0)).currentStepCount.size() < currentSession.MeasurementData.get(dataKeys.get(0)).acc_x.size()){
                Integer number = currentSession.MeasurementData.get(dataKeys.get(0)).currentStepCount.get(currentSession.MeasurementData.get(dataKeys.get(0)).currentStepCount.size()-1);
                currentSession.MeasurementData.get(dataKeys.get(0)).currentStepCount.add(number);
            }
        }

        // Add Measurement Data
        for(int i = 0; i< max_sample_nbr; i++) {

            for (int j = 0; j < dataKeys.size(); j++) {
                if (i<currentSession.MeasurementData.get(dataKeys.get(j)).acc_x.size()){
                    // Timestamp
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).timestamp.get(i)).append(",");
                    // Step count
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).currentStepCount.get(i)).append(",");
                    //measString.append("10").append(",");
                    // Rotation
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).rotAngle_X.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).rotAngle_Y.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).rotAngle_Z.get(i)).append(",");
                    // Acceleration
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).acc_x.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).acc_y.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).acc_z.get(i)).append(",");
                    // Gyroscope
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).gyr_x.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).gyr_y.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).gyr_z.get(i)).append(",");
                    // Magnetometer
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).mag_x.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).mag_y.get(i)).append(",");
                    measString.append(currentSession.MeasurementData.get(dataKeys.get(j)).mag_z.get(i));
                }
                if (j < dataKeys.size() - 1) {
                    measString.append(",");
                } else {
                    measString.append("\n");
                }
            }
        }

        max_sample_nbr = currentSession.CalibrationData.get(calibrationKeys.get(0)).acc_x.size();
        // Add Calibration Headers
        measString.append("\n");
        for(int i = 0; i<calibrationKeys.size(); i++){
            // Get sample size to prevent new loop. Use keys to get data so that the order will be kept the same
            if(max_sample_nbr > currentSession.CalibrationData.get(calibrationKeys.get(i)).acc_x.size()){
                max_sample_nbr = currentSession.CalibrationData.get(calibrationKeys.get(i)).acc_x.size();
            }
            // Timestamp
            measString.append("cal_timestamp_").append(i).append(",");

            // Accelerometer
            measString.append("cal_acc_x_").append(i).append(",");
            measString.append("cal_acc_y_").append(i).append(",");
            measString.append("cal_acc_z_").append(i).append(",");
            // Gyroscope
            measString.append("cal_gyr_x_").append(i).append(",");
            measString.append("cal_gyr_y_").append(i).append(",");
            measString.append("cal_gyr_z_").append(i);

            if(i < dataKeys.size()-1){
                measString.append(",");
            } else {
                measString.append("\n");
            }
        }

        // Add Calibration Data
        for(int i = 0; i< max_sample_nbr; i++) {
            for (int j = 0; j < calibrationKeys.size(); j++) {
                if (i<currentSession.CalibrationData.get(calibrationKeys.get(j)).acc_x.size()){
                    // Timestamp
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).timestamp.get(i)).append(",");
                    // Acceleration
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).acc_x.get(i)).append(",");
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).acc_y.get(i)).append(",");
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).acc_z.get(i)).append(",");
                    // Gyroscope
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).gyr_x.get(i)).append(",");
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).gyr_y.get(i)).append(",");
                    measString.append(currentSession.CalibrationData.get(calibrationKeys.get(j)).gyr_z.get(i));
                }
                if (j < calibrationKeys.size() - 1) {
                    measString.append(",");
                } else {
                    measString.append("\n");
                }
            }
        }



        return measString.toString();
    }



    private void exportDataToCSV() throws IOException {


        StringBuilder csvData = new StringBuilder();
        csvData.append(infoToCSV()).append(measToCSV());

        // Code that saves file to internal directory for savedSessions activity
        File savedSessionsDirectory = new File(getExternalFilesDir(null) + File.separator + GVStore.SavedSessionsFolder);
        if (!savedSessionsDirectory.exists()) {
            if (!savedSessionsDirectory.mkdirs()) {
                Toast.makeText(this, "Something went wrong with the directory creation", Toast.LENGTH_LONG).show();
                finish(); //TODO: proper error handling?
            }
        }

        // Create and write unique file name that doesn't overwrite a possible existing file with the same name
        String uniqueFileName1 = "FileName.csv";
        File file1 = new File(savedSessionsDirectory, uniqueFileName1);
        int index = 1;
        while(file1.exists()){
            uniqueFileName1 = "FileName_" + "(" + index + ").csv";
            file1 = new File(savedSessionsDirectory, uniqueFileName1);
            index += 1;
        }
        FileWriter fileWriter1 = new FileWriter(file1);
        fileWriter1.write(csvData.toString());
        fileWriter1.flush();
        fileWriter1.close();


        // Code that saves file to DOWNLOAD directory for ease of use
        // TODO: Delete this or add make separate buttons for session saving and session export?
        File directory = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
        String uniqueFileName = "FileName.csv";
        File file = new File(directory, uniqueFileName);

        index = 1;
        while(file.exists()){
            uniqueFileName = "FileName_" + "(" + index + ").csv";
            file = new File(directory, uniqueFileName);
            index += 1;
        }

        FileWriter fileWriter = new FileWriter(file);
        fileWriter.write(csvData.toString());
        fileWriter.flush();
        fileWriter.close();
        Toast.makeText(ResultDisplayActivity.this, "File Exported Successfully", Toast.LENGTH_SHORT).show();

    }


    private boolean checkStoragePermission(boolean showNotification) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                if (showNotification) showNotificationAlertToAllowPermission();
                return false;
            }
        } else {
            return true;
        }
    }


    private void showNotificationAlertToAllowPermission() {
        new AlertDialog.Builder(this).setMessage("Please allow Storage Read/Write permission for this app to function properly.").setPositiveButton("Open Settings", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package", getPackageName(), null);
                intent.setData(uri);
                startActivity(intent);
            }
        }).setNegativeButton("Cancel", null).show();

    }

}