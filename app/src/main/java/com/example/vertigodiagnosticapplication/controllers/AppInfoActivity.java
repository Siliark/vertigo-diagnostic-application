package com.example.vertigodiagnosticapplication.controllers;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vertigodiagnosticapplication.R;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.util.Calendar;
import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;

public class AppInfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Element adsElement = new Element();
        adsElement.setTitle("Requirement: \n" + "- Bluetooth connection. \n" + "- Location Access. \n" + "- External storage permission." );

        Element usageInfo = new Element();
        usageInfo.setTitle("Usage: \n" + "- Open the app and enable all the permissions. \n"
                + "- Start automatic session. \n" + "- Follow the Unterberger's test steps for diagnostic sessions. \n" + "- Check the results.");

        View aboutPage = new AboutPage(this)
                .isRTL(false)
                .setDescription("The application provides a portable prototype for your android mobile to diagnose the possible vertigo symstoms based on the Fukuda-Unterberger’s test, " +
                        "with utilizing the sensor data provided by Movesense sensor. \n")
                .addItem(adsElement)
                .addItem(usageInfo)
                .addYoutube("https://www.youtube.com/watch?v=GGMXJ8FWMSw")
                .addGitHub("https://gitlab.com/Siliark/vertigo-diagnostic-application")
                .create();

        setContentView(aboutPage);

        }

}