package com.example.vertigodiagnosticapplication.controllers;

import android.os.Build;
import android.os.Bundle;
import com.example.vertigodiagnosticapplication.R;

import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.MeasurementSession;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@RequiresApi(api = Build.VERSION_CODES.O) // Not quite sure why this is needed. It is needed for the Time function
public class MeasurementSessionSetupActivity extends AppCompatActivity {

    MeasurementSession currentSession = new MeasurementSession(); // Create a new Measurement Session Variable

    AutoCompleteTextView autoCompleteTxt;
    ArrayAdapter<String> adapterItems;

    // Sample rate options
    String[] samplingRates = {"13","26","52","104", "208"};

    // Time variables
    LocalDateTime currentTime;
    static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME; // Used to format the time to a string



    // Main function that gets launched on the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_measurement_session_setup);

        // Set the layout referencing variables
        // Ui
        Button mButtonMeasurementSession = findViewById(R.id.measurement_session_setup_button_StartMeasurement);
        Button mButtonAutomaticMeasurementSessionStart = findViewById(R.id.measurement_session_setup_button_StartAutomaticMeasurement);

        autoCompleteTxt = findViewById(R.id.auto_complete_txt);
        adapterItems = new ArrayAdapter<String>(this, R.layout.misc_simple_item, samplingRates);
        autoCompleteTxt.setAdapter(adapterItems);

        // Listeners and their functions
        autoCompleteTxt.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int item = Integer.parseInt((String) parent.getItemAtPosition(position));
                currentSession.sampleRate = item;
                // could be selectable
                currentSession.measuredVariables = "IMU9";
                Toast.makeText(getApplicationContext(), "Selected Sampling Rate [Hz]: " + item, Toast.LENGTH_SHORT).show(); // Added for demonstration purposes
            }
        });

        // Button listeners
        // Button the start the manual measurement
        mButtonMeasurementSession.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O) // Needed for the now() function of the LocalDateTime object
            @Override
            public void onClick(View view) {
                Intent measurementSessionIntent = new Intent(MeasurementSessionSetupActivity.this, ManualMeasurementSessionActivity.class);

                // Record the current time to use it as an Id for the currentSession
                currentTime = LocalDateTime.now(); // Get the session's time of creation
                GVStore.MeasurementSessionMap.put(currentTime.format(formatter), currentSession); // Store the currentSession in the Global Variable Store
                measurementSessionIntent.putExtra("currentSessionID",currentTime.format(formatter)); // Add the currentSessionID to the intent so that it can be fetched to save the measurements
                startActivity(measurementSessionIntent); // Switch Activity
            }
        });

        // Button the start the automatic measurement
        mButtonAutomaticMeasurementSessionStart.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O) // Needed for the now() function of the LocalDateTime object
            @Override
            public void onClick(View view) {
                Intent measurementSessionIntent = new Intent(MeasurementSessionSetupActivity.this, AutomaticMeasurementSessionActivity.class);
                // Select sensors
                currentSession.rightAnkle = true;
                currentSession.leftAnkle = true;
                currentSession.chest = true;
                //currentSession.chest = true;
                // Select the sampling frequency
                currentSession.sampleRate = 208;
                // Select the measured variables
                currentSession.measuredVariables = "IMU9";
                // Record the current time to use it as an Id for the currentSession
                currentTime = LocalDateTime.now(); // Get the session's time of creation
                GVStore.MeasurementSessionMap.put(currentTime.format(formatter), currentSession); // Store the currentSession in the Global Variable Store
                measurementSessionIntent.putExtra("currentSessionID",currentTime.format(formatter)); // Add the currentSessionID to the intent so that it can be fetched to save the measurements
                startActivity(measurementSessionIntent); // Switch Activity
            }
        });
    }


    // Function to check which sensors should be used for the measurement
    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();
        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkbox_chest:
                currentSession.chest = checked;
                break;
            // Perform your logic
            case R.id.checkbox_leftAnkle:
                Log.e("checkbox", "Pressed" );
                currentSession.leftAnkle = checked;
                Log.e("checkbox", String.valueOf(checked));
                break;
            case R.id.checkbox_rightAnkle:
                currentSession.rightAnkle = checked;
                    break;
        }
    }

}

