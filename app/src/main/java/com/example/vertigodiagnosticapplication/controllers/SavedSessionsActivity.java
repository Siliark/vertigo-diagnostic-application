package com.example.vertigodiagnosticapplication.controllers;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.chaquo.python.Python;
import com.chaquo.python.android.AndroidPlatform;
import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.FileAdapter;

import java.io.File;

public class SavedSessionsActivity extends AppCompatActivity {


    File[] DirFileList;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved_sessions);

        // Start python (Chaquopy) if its not started yet
        if (! Python.isStarted()) {
            Python.start(new AndroidPlatform(this));
        }

        askPermission();
    }


    private void askPermission() {
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,},1); //WRITE_EXTERNAL_STORAGE
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == 1)
        {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                File folder = new File(getExternalFilesDir(null) + File.separator + GVStore.SavedSessionsFolder);
                if (!folder.exists()) {
                    if(!folder.mkdirs()){
                        Toast.makeText(this,"Something went wrong with the directory creation",Toast.LENGTH_LONG).show();
                        finish();
                    }
                }

                DirFileList = folder.listFiles();
                if(DirFileList != null && DirFileList.length > 0){
                    int luku = DirFileList.length;
                    Toast.makeText(this,"There are "+luku+" files in the folder",Toast.LENGTH_LONG).show();

                    TextView NoFilesFoundMessage = findViewById(R.id.NoFilesMessage);
                    NoFilesFoundMessage.setVisibility(View.INVISIBLE);
                    RecyclerView recyclerView = findViewById(R.id.SavedFilesList);
                    recyclerView.setLayoutManager(new LinearLayoutManager(this));
                    recyclerView.setAdapter(new FileAdapter(getApplicationContext(),DirFileList));


                }
                else{
                    TextView NoFilesFoundMessage = findViewById(R.id.NoFilesMessage);
                    NoFilesFoundMessage.setVisibility(View.VISIBLE);

                    Toast.makeText(this,"Something went wrong or there are no files in the folder",Toast.LENGTH_LONG).show();
                }
            }else
            {
                Toast.makeText(this,"To use this feature you have allow access to the file system.",Toast.LENGTH_LONG).show();
                finish();
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }






}
