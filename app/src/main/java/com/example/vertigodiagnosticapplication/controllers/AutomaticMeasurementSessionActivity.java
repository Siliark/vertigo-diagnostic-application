package com.example.vertigodiagnosticapplication.controllers;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.CalibrationData;
import com.example.vertigodiagnosticapplication.models.DataResponse;
import com.example.vertigodiagnosticapplication.models.GyroscopeCaliData;
import com.example.vertigodiagnosticapplication.models.MagnetometerCaliData;
import com.example.vertigodiagnosticapplication.models.MeasurementData;
import com.example.vertigodiagnosticapplication.models.MeasurementSession;
import com.example.vertigodiagnosticapplication.models.SensorSubscription;
import com.google.gson.Gson;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;


public class AutomaticMeasurementSessionActivity extends AppCompatActivity{

    // Get the currentSession to make it easier
    MeasurementSession currentSession;

    // Timer - Needs to be cleaned
    Chronometer automaticSimpleChronometer;
    private long startTime;
    private long endTime;
    private long duration;

    // UI
    public  static TextView automaticStatusUpdate;
    public static Button automaticButton;
    public static TextView automaticStepCount;


    SensorSubscription sensorSubscription = new SensorSubscription();
    public final String measType = "automatic";
    public  final String URI_EVENTLISTENER = "suunto://MDS/EventListener";



    // Main function that gets launched on the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_automatic_measurement_session);
        // Fetch current session from the intent passed by from the setup activity
        currentSession = GVStore.MeasurementSessionMap.get(getIntent().getStringExtra("currentSessionID"));
        automaticSimpleChronometer = findViewById(R.id.automaticSimpleChronometer); // initiate a chronometer

        // Bind the texts and buttons
        automaticStatusUpdate = (TextView) findViewById(R.id.automaticStatusUpdate);
        automaticButton = (Button) findViewById((R.id.automaticButton));
        automaticStepCount = (TextView) findViewById(R.id.automaticStepCount);


        // Sensor subscription URL required by the MDS library
        String URI_MEAS = "/Meas/" + currentSession.measuredVariables + "/" + currentSession.sampleRate; // Defines the measurement string

        // Subscribe to Sensors for the chosen services
        subscribeToSensors(URI_MEAS, currentSession);
    }



    // Loop through the sensor and subscribe to the services
    private void subscribeToSensors(String URI, MeasurementSession currentSession){
        // Looping through the sensors to measure. This should be changed to reflect the choice in the setup.
        for (String serial : GVStore.SensorMap.keySet()) {
            subscribe(serial, URI);
        }
    }




    // Function to start the calibration and the Measurement and update the UI accordingly
    public void automaticCalibrateAndMeasure(View view) {
        if(!GVStore.GyrDone && !GVStore.MagDone && !GVStore.CalDone && !GVStore.MeasDone ){
            // The calibration has not started yet
            automaticButton.setEnabled(false);
            automaticStatusUpdate.setText("Calibrating the Gyroscope");
            GVStore.GyrActive = true;
        }else if(GVStore.GyrDone && !GVStore.MagDone && !GVStore.CalDone && !GVStore.MeasDone){
            // The calibration has not started yet
            automaticButton.setEnabled(false);
            automaticStatusUpdate.setText("Calibrating the Magnetometer");
            GVStore.MagActive = true;
        } else if(GVStore.GyrDone && GVStore.MagDone && !GVStore.CalDone && !GVStore.MeasDone){
            // The Mag calibration has not started yet
            automaticButton.setEnabled(false);
            automaticStatusUpdate.setText("Aligning the Sensors with Gravity");
            GVStore.CalActive = true;
        } else if (GVStore.GyrDone && GVStore.MagDone && GVStore.CalDone && !GVStore.MeasDone){
            // The calibration is done but the measurement has not yet started
            automaticStatusUpdate.setText("Measuring");
            GVStore.MeasActive = true;
            automaticButton.setVisibility(View.GONE);
            automaticSimpleChronometer.setVisibility(View.VISIBLE);
            automaticSimpleChronometer.setBase(SystemClock.elapsedRealtime());
            automaticSimpleChronometer.start();
        } else {
            // Catch the other options that shouldn't occur and reset the UI
            GVStore.MeasActive = false;
            GVStore.CalActive = false;
            GVStore.MagActive = false;
            automaticStatusUpdate.setText("Button Error - Repeat Calibration");
            automaticButton.setEnabled(true);
        }
    }



    // Function that gets called when the "Stop Measurement" button is pressed
    public void stopMeasurement(){

        // Unsubscribe from the sensors - Need to check if it does it for all sensors
        sensorSubscription.unsubscribe();

        // Timing should be fixed and the result added the the currentSession
        automaticSimpleChronometer.stop();
        endTime = System.nanoTime();// start a chronometer
        duration = (SystemClock.elapsedRealtime() - automaticSimpleChronometer.getBase())/1000;

        // Add the final angle to the currentSession - The same should be done for the steps
        double rightAngle =  Math.sqrt(Math.pow(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("rightAnkle")).rotAngle_Z.get(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("rightAnkle")).rotAngle_Z.size() - 1),2));
        double leftAngle =  Math.sqrt(Math.pow(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("leftAnkle")).rotAngle_Z.get(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("leftAnkle")).rotAngle_Z.size() - 1),2));

        currentSession.rotationAngle = (rightAngle + leftAngle) / 2;
        currentSession.testDuration = duration;

        // Change activity and pass on the currentSessionID
        Intent resultDisplayIntent = new Intent(AutomaticMeasurementSessionActivity.this, ResultDisplayActivity.class);
        resultDisplayIntent.putExtra("currentSessionID",getIntent().getStringExtra("currentSessionID"));
        startActivity(resultDisplayIntent); // Switch Activity
    }



    // Function that subscribes to a service of a selected sensor
    public  void subscribe(String connectedSerial, String URI_MEAS) {


        automaticStatusUpdate.setText("Subscribing to " + connectedSerial);

        // Clean up existing subscription (if there is one)
        if (GVStore.SubscriptionMap.get(connectedSerial) != null) {
            GVStore.SubscriptionMap.get(connectedSerial).unsubscribe();
        }

        // Initialize the Measurement and Calibration data variables.
        currentSession.MeasurementData.put(connectedSerial, new MeasurementData());
        currentSession.CalibrationData.put(connectedSerial, new CalibrationData());
        currentSession.MagnetometerCaliData.put(connectedSerial, new MagnetometerCaliData());
        currentSession.GyroscopeCaliData.put(connectedSerial, new GyroscopeCaliData());



        // Build the string used to subscribe to the chosen service
        StringBuilder sb = new StringBuilder();
        String strContract = sb.append("{\"Uri\": \"").append(connectedSerial).append(URI_MEAS).append("\"}").toString();

        // Create the subscription
        GVStore.SubscriptionMap.put(connectedSerial, GVStore.mds_obj.builder().build(this).subscribe(URI_EVENTLISTENER,
                strContract, new MdsNotificationListener() {
                    @Override // Add the code being triggered when receiving a data packet
                    public void onNotification(String data) {

                        // Convert the received Json data to a Java class
                        DataResponse dataResponse = new Gson().fromJson(data, DataResponse.class);
                        // Fetch the serial of the sensor
                        String packetSerial = dataResponse.uri.split("/")[0];


                        // If all sensors have been calibrated the calibration mode is desactivated
                        if(GVStore.gyrCalibrationCount == ((currentSession.chest? 1 : 0) + (currentSession.rightAnkle? 1 : 0) + (currentSession.leftAnkle? 1 : 0)))
                        {
                            AutomaticMeasurementSessionActivity.automaticButton.setText("Start Magnetometer Calibration");
                            AutomaticMeasurementSessionActivity.automaticStatusUpdate.setText("Gyroscope Calibrated");
                            AutomaticMeasurementSessionActivity.automaticButton.setEnabled(true);
                            GVStore.GyrActive = false;
                            GVStore.GyrDone = true;
                            GVStore.gyrCalibrationCount = 0;
                        }

                        // If all sensors have been calibrated the calibration mode is desactivated
                        if(GVStore.magCalibrationCount == ((currentSession.chest? 1 : 0) + (currentSession.rightAnkle? 1 : 0) + (currentSession.leftAnkle? 1 : 0)))
                        {
                            AutomaticMeasurementSessionActivity.automaticButton.setText("Start Alignment Calibration");
                            AutomaticMeasurementSessionActivity.automaticStatusUpdate.setText("Magnetometer Calibrated");
                            AutomaticMeasurementSessionActivity.automaticButton.setEnabled(true);
                            GVStore.MagActive = false;
                            GVStore.MagDone = true;
                            GVStore.magCalibrationCount = 0;
                        }


                        // If all sensors have been calibrated the calibration mode is desactivated
                        if(GVStore.calibrationCount == ((currentSession.chest? 1 : 0) + (currentSession.rightAnkle? 1 : 0) + (currentSession.leftAnkle? 1 : 0)))
                        {
                            AutomaticMeasurementSessionActivity.automaticButton.setText("Start Measurement");
                            AutomaticMeasurementSessionActivity.automaticStatusUpdate.setText("Calibration Done");
                            AutomaticMeasurementSessionActivity.automaticButton.setEnabled(true);
                            GVStore.CalActive = false;
                            GVStore.CalDone = true;
                            GVStore.calibrationCount = 0;
                        }

                        // code to stop the measurement in the automatic setup if the 50 steps have been reached
                        if(currentSession.stepCount == 30 && currentSession.MeasurementData.get(GVStore.SensorPlacement.get("leftAnkle")).stepping == false && currentSession.MeasurementData.get(GVStore.SensorPlacement.get("leftAnkle")).stepping ==false){
                            //The measurement is undergoing and is being stopped
                            GVStore.MeasActive = false;
                            GVStore.MeasDone = true;
                            automaticStatusUpdate.setText("Processing Measurement Data");
                            stopMeasurement();
                        }


                        // In case the magnetometer calibrating mode is active run this code
                        if(GVStore.GyrActive){
                            if (dataResponse.body.acc_array.length > 0) {
                                sensorSubscription.gatherGyroscopeCalibrationData(currentSession, dataResponse, packetSerial);
                            }
                        }

                        // In case the magnetometer calibrating mode is active run this code
                        if(GVStore.MagActive){
                            if (dataResponse.body.acc_array.length > 0) {
                                sensorSubscription.gatherMagnetometerCalibrationData(currentSession, dataResponse, packetSerial);
                            }
                        }

                        // In case the calibrating mode is active run this code
                        if(GVStore.CalActive){
                            if (dataResponse.body.acc_array.length > 0) {
                                sensorSubscription.gatherCalibrationData(currentSession, dataResponse, packetSerial);
                            }
                        }

                        // In case the measurement mode is active run this code
                        if(GVStore.MeasActive) {
                            if (dataResponse.body.acc_array.length > 0) {
                                sensorSubscription.gatherMeasurementData(currentSession, dataResponse, packetSerial);
                            }
                        }

                        // Update steps
                        automaticStepCount.setText(String.valueOf(currentSession.stepCount));


                    }
                    @Override
                    public void onError(MdsException error) {
                        Log.e("LOG_TAG", "subscription onError(): ", error);
                        sensorSubscription.unsubscribe();
                    }
                })
        );

        // Update to the status text with the subscribed sensor
        automaticStatusUpdate.setText("Subscribed to: " + connectedSerial);

        // Check if all sensors are subscribed too and update the status text accordingly
        GVStore.subscriptionCount++;
        if(GVStore.subscriptionCount == ((currentSession.chest? 1 : 0) + (currentSession.rightAnkle? 1 : 0) + (currentSession.leftAnkle? 1 : 0))){
            GVStore.subscriptionCount = 0;
            automaticStatusUpdate.setText("Start the calibration");
            automaticButton.setEnabled(true);
        }
    }





    // This function catches the back button of the phone. This is just to test as ideally all back buttons should clear the variables appropriately.
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        AutomaticMeasurementSessionActivity.super.onBackPressed();
                    }
                }).create().show();
    }



}



