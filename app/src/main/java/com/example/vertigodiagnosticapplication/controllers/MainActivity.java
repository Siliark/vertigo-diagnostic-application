package com.example.vertigodiagnosticapplication.controllers;

import android.Manifest;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.movesense.mds.Mds;
import com.example.vertigodiagnosticapplication.utils.ScannerFragment;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.polidea.rxandroidble2.RxBleConnection;
import com.polidea.rxandroidble2.RxBleDevice;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.annotations.NonNull;
import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;


public class MainActivity extends AppCompatActivity implements ScannerFragment.DeviceSelectionListener {

    // Initialise the variables referencing the layout
    private Button mButtonMeasurementSessionSetup;
    private Button mButtonDrawGraph;
    private Button mButtonInfo;
    // Not yet used
    private Button mButtonFAQ;
    private Button mButtonSaveData;
    private Button mButtonSavedSessions;

    // Binding views with butterknife
    // LeftSensor binding (One could change the name of the variables
    @BindView(R.id.add_sensor_left_knee) LinearLayout addSensorLeftKnee;
    @BindView(R.id.left_knee_tv) TextView LeftKneeTv;
    @BindView(R.id.left_knee_device_name) TextView LeftKneeName;
    @BindView(R.id.left_knee_device_info) LinearLayout LeftKneeInfo;
    @BindView(R.id.left_status) TextView LeftStatus;
    @BindView(R.id.disconnectButtonLeft) FloatingActionButton disconnectButtonLeft;
    // RightSensor
    @BindView(R.id.add_sensor_right_knee) LinearLayout addSensorRightKnee;
    @BindView(R.id.right_knee_tv) TextView RightKneeTv;
    @BindView(R.id.right_knee_device_name) TextView RightKneeName;
    @BindView(R.id.right_knee_device_info) LinearLayout RightKneeInfo;
    @BindView(R.id.right_status) TextView RightStatus;
    @BindView(R.id.disconnectButtonRight) FloatingActionButton  disconnectButtonRight;
    // ChestSensor
    @BindView(R.id.add_sensor_chest) LinearLayout addSensorChest;
    @BindView(R.id.chest_tv) TextView ChestTv;
    @BindView(R.id.chest_device_name) TextView ChestName;
    @BindView(R.id.chest_device_info) LinearLayout ChestInfo;
    @BindView(R.id.chest_status) TextView ChestStatus;
    @BindView(R.id.disconnectButtonChest) FloatingActionButton  disconnectButtonChest;

    // Variable needed for the scanning and connecting of sensors
    private ScannerFragment scannerFragment; // Scanner variable (Don't really know what it does - needs further research)
    // These Variables are needed to check which Sensor location is chosen
    private boolean isAddLeftKneePressed = false;
    private boolean isAddRightKneePressed = false;
    private boolean isAddChestPressed = false;

    private final int REQUEST_LOCATION_PERMISSION = 1;


    // Main function that gets launched at the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        // // Make sure we have all the permissions this app needs
        requestLocationPermissions();
        // Initializing the Movesense Library
        initMdsObj();

        // Set the layout referencing variables could potentially be done in a different way since Butterknife was added.
        mButtonMeasurementSessionSetup = findViewById(R.id.main_button_StartMeasuringSession);
        mButtonInfo = findViewById(R.id.main_button_AppInfo);;
        mButtonSavedSessions = findViewById(R.id.main_button_SavedSessions);

        // Listeners and their functions
        // Button listeners
        mButtonMeasurementSessionSetup.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                Intent measurementSessionSetupIntent = new Intent(MainActivity.this, MeasurementSessionSetupActivity.class);
                startActivity(measurementSessionSetupIntent); //Switching activity
            }
        });

        mButtonInfo.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                Intent AppInfoIntent = new Intent(MainActivity.this, AppInfoActivity.class);
                startActivity(AppInfoIntent); //Switching activity
            }
        });

        mButtonSavedSessions.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onClick(View view) {
                Intent SavedSessionIntent = new Intent(MainActivity.this, SavedSessionsActivity.class);
                startActivity(SavedSessionIntent); //Switching activity
            }
        });

        ButterKnife.bind(this); // Activate Butterknife
        writeDeviceInfo(); // Write the info of the selected and connected devices

        // This has no use but is kept for reference so that I remember it can be done
        //if (getSupportActionBar() != null) {
        //    getSupportActionBar().setTitle("Multi Connection");
        //}
    }



    // Function used to save the Movesense library to the GlobalVariable Store
    public void initMdsObj() {
        GVStore.mds_obj = Mds.builder().build(this);
    }


@Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @AfterPermissionGranted(REQUEST_LOCATION_PERMISSION)
    public void requestLocationPermissions() {
        String[] perms = {Manifest.permission.ACCESS_FINE_LOCATION};
        if(EasyPermissions.hasPermissions(this, perms)) {
            Toast.makeText(this, "Permission already granted", Toast.LENGTH_SHORT).show();
        }
        else {
            EasyPermissions.requestPermissions(this, "Please grant the location permission", REQUEST_LOCATION_PERMISSION, perms);
        }
    }



    // Function that populates the connected devices. This is used at the creation of the activity in case one navigates back to it
    private void writeDeviceInfo(){
        if (!GVStore.SensorPlacement.get("leftAnkle").equals("Unassigned")){
            LeftKneeTv.setVisibility(View.GONE);
            LeftKneeInfo.setVisibility(View.VISIBLE);
            LeftKneeName.setText(GVStore.SensorMap.get(GVStore.SensorPlacement.get("leftAnkle")).getName());
            // Check the connectivity status
            if(GVStore.SensorMap.get(GVStore.SensorPlacement.get("leftAnkle")).getConnectionState() == RxBleConnection.RxBleConnectionState.CONNECTED){
                LeftStatus.setText("Connected");
            }
        }
        if (!GVStore.SensorPlacement.get("rightAnkle").equals("Unassigned")){
            RightKneeTv.setVisibility(View.GONE);
            RightKneeInfo.setVisibility(View.VISIBLE);
            RightKneeName.setText(GVStore.SensorMap.get(GVStore.SensorPlacement.get("rightAnkle")).getName());
            // Check the connectivity status
            if(GVStore.SensorMap.get(GVStore.SensorPlacement.get("rightAnkle")).getConnectionState() == RxBleConnection.RxBleConnectionState.CONNECTED){
                RightStatus.setText("Connected");
            }
        }
        if (!GVStore.SensorPlacement.get("chest").equals("Unassigned")){
            ChestTv.setVisibility(View.GONE);
            ChestInfo.setVisibility(View.VISIBLE);
            ChestName.setText(GVStore.SensorMap.get(GVStore.SensorPlacement.get("chest")).getName());
            // Check the connectivity status
            if(GVStore.SensorMap.get(GVStore.SensorPlacement.get("chest")).getConnectionState() == RxBleConnection.RxBleConnectionState.CONNECTED){
                ChestStatus.setText("Connected");
            }
        }
    }



    // Function that connects to the selected BLEDevice
    private void connectBLEDevice(RxBleDevice device) {
        RxBleDevice bleDevice = device; // Not sure if this is necessary
        GVStore.mds_obj.connect(bleDevice.getMacAddress(), new MdsConnectionListener() {
            @Override
            public void onConnect(String s) {
            }
            @Override
            public void onConnectionComplete(String macAddress, String serial) {
                // Checks which device was connected and updates it's info text
                if(GVStore.SensorPlacement.get("chest").equals(serial)){
                    ChestStatus.setText("Connected"); // Update text
                }else if(GVStore.SensorPlacement.get("rightAnkle").equals(serial)){
                    RightStatus.setText("Connected"); // Update text
                }else if(GVStore.SensorPlacement.get("leftAnkle").equals(serial)){
                    LeftStatus.setText("Connected"); // Update text
                }
                // For the moment this isn't used. The idea is to have a counter so that one can wait until all sensors are connected before going to the next activity
                GVStore.connectingCount++;
            }
            @Override
            public void onError(MdsException e) {
            }
            @Override
            public void onDisconnect(String bleAddress) {
            }
        });
    }



    // Selection functions
    // Not sure if needed
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }



    // Function that checks if the selected placement has a sensor already connected. If it does it disconnects from it and cleans the variables and UI.
    // Afterwards it starts the scanning - Not quite sure how this scanning implementation works
    @OnClick({ R.id.add_sensor_left_knee, R.id.add_sensor_right_knee, R.id.add_sensor_chest})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.add_sensor_left_knee:
                if(!GVStore.SensorPlacement.get("leftAnkle").equals("Unassigned")){
                    // Disconnecting from sensor
                    GVStore.mds_obj.disconnect(GVStore.SensorMap.get(GVStore.SensorPlacement.get("leftAnkle")).getMacAddress());
                    // Cleaning the variables and UI
                    GVStore.SensorMap.remove(GVStore.SensorPlacement.get("leftAnkle")); // The order matters
                    GVStore.SensorPlacement.replace("leftAnkle", "Unassigned");
                    LeftKneeInfo.setVisibility(View.GONE);
                    disconnectButtonLeft.setVisibility(View.GONE);
                    LeftKneeTv.setVisibility(View.VISIBLE);
                    LeftStatus.setText("Disconnected");
                }
                // Start the scanning
                scannerFragment = new ScannerFragment();
                scannerFragment.show(getSupportFragmentManager(), ScannerFragment.class.getName());
                isAddLeftKneePressed = true; // Tells which location the future selected device belongs too
                break; // Break is necessary to avoid running all the cases

            case R.id.add_sensor_right_knee:
                if(!GVStore.SensorPlacement.get("rightAnkle").equals("Unassigned")){
                    // Disconnecting from sensor
                    GVStore.mds_obj.disconnect(GVStore.SensorMap.get(GVStore.SensorPlacement.get("rightAnkle")).getMacAddress());
                    // Cleaning the variables and UI
                    GVStore.SensorMap.remove(GVStore.SensorPlacement.get("rightAnkle")); // The order matters
                    GVStore.SensorPlacement.replace("rightAnkle", "Unassigned");
                    RightKneeInfo.setVisibility(View.GONE);
                    disconnectButtonRight.setVisibility(View.GONE);
                    RightKneeTv.setVisibility(View.VISIBLE);
                    RightStatus.setText("Disconnected");
                }
                // Start the scanning
                scannerFragment = new ScannerFragment();
                scannerFragment.show(getSupportFragmentManager(), ScannerFragment.class.getName());
                isAddRightKneePressed = true; // Tells which location the future selected device belongs too
                break; // Break is necessary to avoid running all the cases

            case R.id.add_sensor_chest:
                if(!GVStore.SensorPlacement.get("chest").equals("Unassigned")){
                    // Disconnecting from sensor
                    GVStore.mds_obj.disconnect(GVStore.SensorMap.get(GVStore.SensorPlacement.get("chest")).getMacAddress());
                    // Cleaning the variables and UI
                    GVStore.SensorMap.remove(GVStore.SensorPlacement.get("chest")); // The order matters
                    GVStore.SensorPlacement.replace("chest", "Unassigned");
                    ChestInfo.setVisibility(View.GONE);
                    disconnectButtonChest.setVisibility(View.GONE);
                    ChestTv.setVisibility(View.VISIBLE);
                    ChestStatus.setText("Disconnected");
                }
                // Start the scanning
                scannerFragment = new ScannerFragment();
                scannerFragment.show(getSupportFragmentManager(), ScannerFragment.class.getName());
                isAddChestPressed = true; // Tells which location the future selected device belongs too
                break; // Break is necessary to avoid running all the cases
        }
    }



    // Function that Connects to the selected device and updates the UI to display its info
    @Override
    public void onDeviceSelected(RxBleDevice device) {
        // Calculate the serial of the device that will be used as a key throughout the system. (Was chosen because it is physically printed on the Movesense device)
        String serial = device.getName().split("\\s+")[1];

        scannerFragment.dismiss(); // Not sure what this does

        // Checks which location was selected
        if (isAddRightKneePressed){
            isAddRightKneePressed = false; // reset the selection variable
            // Add the device to the Global Variable Store
            GVStore.SensorPlacement.replace("rightAnkle", serial);
            GVStore.SensorMap.put(serial, device);
            // Update the UI
            RightKneeTv.setVisibility(View.GONE);
            RightKneeInfo.setVisibility(View.VISIBLE);
            disconnectButtonRight.setVisibility(View.VISIBLE);
            RightKneeName.setText(device.getName());
            RightStatus.setText("Connecting");
            // Connect to sensor
            connectBLEDevice(device);
        }
        else if (isAddLeftKneePressed){
            isAddLeftKneePressed = false; // reset the selection variable
            // Add the device to the Global Variable Store
            GVStore.SensorPlacement.replace("leftAnkle", serial);
            GVStore.SensorMap.put(serial, device);
            // Update the UI
            LeftKneeTv.setVisibility(View.GONE);
            LeftKneeInfo.setVisibility(View.VISIBLE);
            disconnectButtonLeft.setVisibility(View.VISIBLE);
            Log.e("TAG", device.getName());
            Log.e("TAG", device.getMacAddress());

            LeftKneeName.setText(device.getName());
            LeftStatus.setText("Connecting");
            // Connect to sensor
            connectBLEDevice(device);
        }
        else if (isAddChestPressed){
            isAddChestPressed = false; // reset the selection variable
            // Add the device to the Global Variable Store
            GVStore.SensorPlacement.replace("chest", serial);
            GVStore.SensorMap.put(serial, device);
            // Update the UI
            ChestTv.setVisibility(View.GONE);
            ChestInfo.setVisibility(View.VISIBLE);
            disconnectButtonChest.setVisibility(View.VISIBLE);
            ChestName.setText(device.getName());
            ChestStatus.setText("Connecting");
            // Connect to sensor
            connectBLEDevice(device);
        }
    }



    // Function that disconnects from the sensors when the disconnect button is pressed and clears the variables and UI
    @OnClick({ R.id.disconnectButtonChest, R.id.disconnectButtonRight, R.id.disconnectButtonLeft})
    public void onDisconnectButton(View view) {
        // Checks which button was pressed
        switch (view.getId()) {
            case R.id.disconnectButtonChest:
                // Disconnecting from sensor
                GVStore.mds_obj.disconnect(GVStore.SensorMap.get(GVStore.SensorPlacement.get("chest")).getMacAddress());
                // Cleaning the variables and UI
                GVStore.SensorMap.remove(GVStore.SensorPlacement.get("chest")); // The order matters
                GVStore.SensorPlacement.replace("chest", "Unassigned");
                ChestInfo.setVisibility(View.GONE);
                disconnectButtonChest.setVisibility(View.GONE);
                ChestTv.setVisibility(View.VISIBLE);
                ChestStatus.setText("Disconnected");
                break;

            case R.id.disconnectButtonRight:
                // Disconnecting from sensor
                GVStore.mds_obj.disconnect(GVStore.SensorMap.get(GVStore.SensorPlacement.get("rightAnkle")).getMacAddress());
                // Cleaning the variables and UI
                GVStore.SensorMap.remove(GVStore.SensorPlacement.get("rightAnkle")); // The order matters
                GVStore.SensorPlacement.replace("rightAnkle", "Unassigned");
                RightKneeInfo.setVisibility(View.GONE);
                disconnectButtonRight.setVisibility(View.GONE);
                RightKneeTv.setVisibility(View.VISIBLE);
                RightStatus.setText("Disconnected");
                break;

            case R.id.disconnectButtonLeft:
                // Disconnecting from sensor
                GVStore.mds_obj.disconnect(GVStore.SensorMap.get(GVStore.SensorPlacement.get("leftAnkle")).getMacAddress());
                // Cleaning the variables and UI
                GVStore.SensorMap.remove(GVStore.SensorPlacement.get("leftAnkle")); // The order matters
                GVStore.SensorPlacement.replace("leftAnkle", "Unassigned");
                LeftKneeInfo.setVisibility(View.GONE);
                disconnectButtonLeft.setVisibility(View.GONE);
                LeftKneeTv.setVisibility(View.VISIBLE);
                LeftStatus.setText("Disconnected");
                break;
        }
    }



}