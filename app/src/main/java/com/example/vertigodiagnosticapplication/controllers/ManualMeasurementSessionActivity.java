package com.example.vertigodiagnosticapplication.controllers;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.vertigodiagnosticapplication.R;
import com.example.vertigodiagnosticapplication.global_variables.GVStore;
import com.example.vertigodiagnosticapplication.models.CalibrationData;
import com.example.vertigodiagnosticapplication.models.DataResponse;
import com.example.vertigodiagnosticapplication.models.MeasurementData;
import com.example.vertigodiagnosticapplication.models.MeasurementSession;
import com.example.vertigodiagnosticapplication.models.SensorSubscription;
import com.google.gson.Gson;
import com.movesense.mds.MdsConnectionListener;
import com.movesense.mds.MdsException;
import com.movesense.mds.MdsNotificationListener;
import com.polidea.rxandroidble2.RxBleDevice;


public class ManualMeasurementSessionActivity extends AppCompatActivity {

    // Get the currentSession to make it easier
    MeasurementSession currentSession;

    // Timer - Needs to be cleaned
    Chronometer simpleChronometer;
    private long startTime;
    private long endTime;
    private long duration;

    // UI
    public static TextView StepCountLabel;
    public static TextView statusUpdate;
    public static Button Manual_CalibrateButton;
    public static Button Manual_MeasureButton;

    public  final String URI_EVENTLISTENER = "suunto://MDS/EventListener";
    public final String measType = "manual";
    SensorSubscription sensorSubscription = new SensorSubscription();



    // Main function that gets launched on the creation of this activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manual_measurement_session);

        simpleChronometer = findViewById(R.id.simpleChronometer); // initiate a chronometer

        // Fetch current session from the intent passed by from the setup activity

        // Bind the texts and buttons
        statusUpdate = (TextView) findViewById(R.id.statusUpdate);
        StepCountLabel = (TextView) findViewById(R.id.StepCountLabel);
        Manual_CalibrateButton = (Button) findViewById((R.id.Manual_CalibrateButton));
        Manual_MeasureButton = (Button) findViewById((R.id.Manual_MeasureButton));

        currentSession = GVStore.MeasurementSessionMap.get(getIntent().getStringExtra("currentSessionID"));

        // Sensor subscription URL required by the MDS library
        String URI_MEAS = "/Meas/" + currentSession.measuredVariables + "/" + currentSession.sampleRate; // Defines the measurement string

        // Subscribe to Sensors for the chosen services
        subscribeToSensors(URI_MEAS, currentSession);
    }



    // Loop through the sensor and subscribe to the services
    private void subscribeToSensors(String URI, MeasurementSession currentSession){
        // Looping through the sensors to measure. This should be changed to reflect the choice in the setup.
        for (String serial : GVStore.SensorMap.keySet()) {
            subscribe(serial, URI);
        }
    }



    // Function to start the calibration and the Measurement and update the UI accordingly
    public void calibrateAndMeasure(View view) {
        if(!GVStore.CalActive && !GVStore.MeasActive && !GVStore.CalDone && !GVStore.MeasDone){
            // The calibration has not started yet
            Manual_CalibrateButton.setEnabled(false);
            statusUpdate.setText("Calibrating");
            GVStore.CalActive = true;
        } else if (!GVStore.CalActive && !GVStore.MeasActive && GVStore.CalDone && !GVStore.MeasDone){
            // The calibration is done but the measurement has not yet started
            Log.e("test", "calibrateAndMeasure:");
            statusUpdate.setText("Measuring");
            GVStore.MeasActive = true;
            Manual_CalibrateButton.setEnabled(false);
            Manual_MeasureButton.setText("Stop Measurement");
            StepCountLabel.setVisibility(View.VISIBLE);
            simpleChronometer.setVisibility(View.VISIBLE);
            simpleChronometer.setBase(SystemClock.elapsedRealtime());
            simpleChronometer.start();
        } else if (!GVStore.CalActive && GVStore.MeasActive && GVStore.CalDone && !GVStore.MeasDone){
            // The measurement is undergoing and is being stopped
            Manual_CalibrateButton.setEnabled(false);
            Manual_MeasureButton.setEnabled(false);
            GVStore.MeasActive = false;
            GVStore.MeasDone = true;
            statusUpdate.setText("Processing Measurement Data");
            stopMeasurement();
        } else {
            // Catch the other options that shouldn't occur and reset the UI
            GVStore.MeasActive = false;
            GVStore.CalActive = false;
            statusUpdate.setText("Button Error - Repeat Calibration");
            Manual_CalibrateButton.setEnabled(true);
            Manual_MeasureButton.setEnabled(false);
        }
    }



    // Function that gets called when the "Stop Measurement" button is pressed
    public void stopMeasurement(){

        // Unsubscribe from the sensors - Need to check if it does it for all sensors
        sensorSubscription.unsubscribe();

        // Timing should be fixed and the result added the the currentSession
        simpleChronometer.stop();
        StepCountLabel.setVisibility(View.GONE);
        simpleChronometer.setVisibility(View.GONE);

        endTime = System.nanoTime();// start a chronometer
        duration = (endTime-startTime)/1000000000;

        // Calculate angle
        double angle = (Math.sqrt(Math.pow(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("rightAnkle")).rotAngle_Z.get(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("rightAnkle")).rotAngle_Z.size() - 1),2)) +
                Math.sqrt(Math.pow(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("leftAnkle")).rotAngle_Z.get(currentSession.MeasurementData.get(GVStore.SensorPlacement.get("leftAnkle")).rotAngle_Z.size() - 1),2)))/2;
        // Add the final angle to the currentSession - The same should be done for the steps
        currentSession.rotationAngle = angle
        ;

        // Change activity and pass on the currentSessionID
        Intent resultDisplayIntent = new Intent(ManualMeasurementSessionActivity.this, ResultDisplayActivity.class);
        resultDisplayIntent.putExtra("currentSessionID",getIntent().getStringExtra("currentSessionID"));
        startActivity(resultDisplayIntent); // Switch Activity
    }



    // Function that subscribes to a service of a selected sensor
    public  void subscribe(String connectedSerial, String URI_MEAS) {


        statusUpdate.setText("Subscribing to " + connectedSerial);

        // Clean up existing subscription (if there is one)
        if (GVStore.SubscriptionMap.get(connectedSerial) != null) {
            GVStore.SubscriptionMap.get(connectedSerial).unsubscribe();
        }

        // Initialize the Measurement and Calibration data variables.
        currentSession.MeasurementData.put(connectedSerial, new MeasurementData());
        currentSession.CalibrationData.put(connectedSerial, new CalibrationData());

        // Build the string used to subscribe to the chosen service
        StringBuilder sb = new StringBuilder();
        String strContract = sb.append("{\"Uri\": \"").append(connectedSerial).append(URI_MEAS).append("\"}").toString();

        // Create the subscription
        GVStore.SubscriptionMap.put(connectedSerial, GVStore.mds_obj.builder().build(this).subscribe(URI_EVENTLISTENER,
                strContract, new MdsNotificationListener() {
                    @Override // Add the code being triggered when receiving a data packet
                    public void onNotification(String data) {

                        // Convert the received Json data to a Java class
                        DataResponse dataResponse = new Gson().fromJson(data, DataResponse.class);
                        // Fetch the serial of the sensor
                        String packetSerial = dataResponse.uri.split("/")[0];

                        // If all sensors have been calibrated the calibration mode is deactivated
                        if(GVStore.calibrationCount == ((currentSession.chest? 1 : 0) + (currentSession.rightAnkle? 1 : 0) + (currentSession.leftAnkle? 1 : 0)))
                        {
                            ManualMeasurementSessionActivity.statusUpdate.setText("Calibration Done");
                            ManualMeasurementSessionActivity.Manual_CalibrateButton.setText("Recalibrate");
                            ManualMeasurementSessionActivity.Manual_CalibrateButton.setEnabled(true);
                            ManualMeasurementSessionActivity.Manual_MeasureButton.setEnabled(true);

                            GVStore.CalActive = false;
                            GVStore.CalDone = true;
                            GVStore.calibrationCount = 0;
                        }

                        // In case the calibrating mode is active run this code
                        if(GVStore.CalActive){
                            if (dataResponse.body.acc_array.length > 0) {
                                sensorSubscription.gatherCalibrationData(currentSession, dataResponse, packetSerial);
                            }
                        }

                        // In case the measurement mode is active run this code
                        if(GVStore.MeasActive) {
                            if (dataResponse.body.acc_array.length > 0) {
                                sensorSubscription.gatherMeasurementData(currentSession, dataResponse, packetSerial);
                            }
                        }
                        // Update steps
                        StepCountLabel.setText(String.valueOf(currentSession.stepCount));

                    }
                    @Override
                    public void onError(MdsException error) {
                        Log.e("LOG_TAG", "subscription onError(): ", error);
                        sensorSubscription.unsubscribe();
                    }
                })
        );

        // Update to the status text with the subscribed sensor
        statusUpdate.setText("Subscribed to: " + connectedSerial);

        // Check if all sensors are subscribed too and update the status text accordingly
        GVStore.subscriptionCount++;
        if(GVStore.subscriptionCount == ((currentSession.chest? 1 : 0) + (currentSession.rightAnkle? 1 : 0) + (currentSession.leftAnkle? 1 : 0))){
            GVStore.subscriptionCount = 0;
            statusUpdate.setText("Sensor Subscription Complete");
            Manual_CalibrateButton.setEnabled(true);
        }
    }



    // This function catches the back button of the phone. This is just to test as ideally all back buttons should clear the variables appropriately.
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setTitle("Really Exit?")
                .setMessage("Are you sure you want to exit?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        ManualMeasurementSessionActivity.super.onBackPressed();
                    }
                }).create().show();
    }
}
