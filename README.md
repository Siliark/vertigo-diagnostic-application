# Vertigo Diagnostic Application - Android App

The application provides a portable prototype for your android mobile to diagnose the
possible vertigo symptoms based on the Fukuda-Unterberger’s test, with utilizing the sensor data provided by Movesense sensor.

## About

Vertigo is described as the sensation of movement, or the movement of surrounding objects without any actual movement.
It is a symptom that may be associated with others such as nausea, vomiting, sweating, or difficulties walking and has various distinct potential causes

## Features

The android app lets you:

- Connect different multiple Movesense sensors via Bluetooth
- To have the most appropriate tests, three Movesense sensors could be used.
- These sensors can be placed on left, right ankles and chest
- Based on the Fukuda-Unterberger’s test, the app requires patients to take several steps without moving from the spot while having their eyes closed.
- After the tests, the app will show how many steps that the patients have been used during diagnostic sessions, and how they rotate their bodies after the tests.
- The app currently supports two modes which are manual and automatic measurements.
  - The automatic measurement provides a more comfortable and convenient way to perform the vertigo diagnostic sessions, and it is more suitable to be used by the clinicians, doctors, or regular users.
  - The manual measurement are only used for maintenance purposes.

## Screenshots

## Build Instructions

Android studio is required to build the application. To run the application do the following:

- Open the project in android studio.
- Trust/give permission for android studio to synchronize the project
- Give the Python SDK (Chaquopy) the path to the python.exe. This is done in module level build.gradle file inside the Python{} by using the buildPython command.
- Run/build the application using android studio.

Things to note and potential problems:

- When the application is build for the first time android studio has to download and install all the necessary features. This can take a long time, even tens of minutes in lower end computers.
- Sometimes for unknown reason, some of the third-party libraries aren't initialized properly when the application is opened for the first time. This should be fixed by resynchronizing the module level build.gradle file.
- The application cannot be ran inside the android studio emulator. This is because it required bluetooth which isn't available inside the emulator.


## App Instructions

Automatic measurement

- Opening the app
- The application will ask for the permissions to access location and external storage.
- Accept the request and connect the android mobile device with Bluetooth and GPS Location.
- Select start measurement sessions, and the data sample rate
  - The app will automatically detect and connect available Movesense sensors (maximum 3 sensors)
    and activate the timers for the test session.
- The patients their eyes and perform the Fukuda-Unterberger’s test.
- When the measurement session ends, the tests will show the latest step counts, rotation angles (both in numbers and graph) and save them to specific CSV files in the mobile storage.

Things to note:

- Currently the manual measurement sessions isn't working. This is because it currently uses previous sensor calibration features that have since been removed or modified to be incompatible with it.
- Currently to use python for graphing we are using unlicensed Chaquopy. This means that when graphing functionality is started, the app will show a message about it and can only ran for 5 minutes before forcibly restarting.


## Permissions

The app works for the Android versions after to Android 6.0 and it requires the following permissions:

- Mobile data or wireless connections.
- Mobile external storage permission.
- Location Permissions.

The "Run at startup" permission is only used if Auto-Sync feature is enabled and is not utilised otherwise. The network access permissions are made use of for downloading content. The external storage permission is used to cache article images for viewing offline.

## Contributing

Vertigo Diagnostic Application app is a free and open source project developed by Aalto Univeristy students. Any contributions are welcome. Here are a few ways you can help:

- [Report bugs and make suggestions.](https://github.com/wallabag/android-app/issues)
- When writing new codes, please follow the code style used in the project to make a review process faster.

## License

This app uses open-source libraries provided by Movesense to implement some basic functionalities, such as connecting their sensors via Bluetooth, sending and receiving data from the sensors.

However, our application has a different UI, code structures and especially we implement new functionalities for the diagnosis of vertigo such as step counting, angle measurement, data processing and management.

## Contact

| Contacts     |  Email or Gitlab's account  |
| ------------ | :-------------------------: |
| Daniel Fasel | daniel.fasel@protonamil.com |
| Danh Nguyen  |    caodanh1802@gmail.com    |
| Tero Rontti  |    tero.rontti@aalto.fi     |
| Radu Cosmina |    cosmina.radu@aalto.fi    |

## App Reference

- [Movesense API Reference.](https://www.movesense.com/docs/mobile/android/main/)
